from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from urllib.parse import parse_qs
import json

connected_users_9 = set()
event_started_9 = False


class QuizConsumer9(AsyncWebsocketConsumer):
    event_scheduled = False
    group_name = 'quiz_users_9'

    async def connect(self):
        query_params = parse_qs(self.scope['query_string'].decode('utf-8'))
        token = query_params.get('token', [None])[0]
        user = await self.authenticate_user(token)

        if user:
            if user.pk in connected_users_9:
                await self.channel_layer.group_discard(
                    self.group_name,
                    self.channel_name
                )

            self.scope['user'] = user.pk
            connected_users_9.add(user.pk)
            await self.channel_layer.group_add(
                self.group_name,
                self.channel_name
            )
            await self.accept()

            emails = await self.get_usernames(list(connected_users_9))
            await self.send_message("user_list", emails)
        else:
            await self.close()

    @database_sync_to_async
    def authenticate_user(self, token):
        from rest_framework_simplejwt.tokens import AccessToken
        from custom_auth.models import CustomUser

        try:
            access_token = AccessToken(token)
            user_id = access_token['user_id']
            user = CustomUser.objects.get(pk=user_id)
            return user
        except CustomUser.DoesNotExist:
            return None

    @database_sync_to_async
    def get_usernames(self, user_ids):
        from custom_auth.models import CustomUser
        users = CustomUser.objects.filter(pk__in=user_ids)
        return [user.username for user in users]

    async def disconnect(self, close_code=None):
        user_pk = self.scope.get('user', None)
        if user_pk in connected_users_9:
            connected_users_9.remove(user_pk)

        emails = await self.get_usernames(list(connected_users_9))
        await self.send_message("user_list", emails)

        await self.channel_layer.group_discard(
            self.group_name,
            self.channel_name
        )

    # New generic method for sending messages
    async def send_message(self, message_type, data):
        await self.channel_layer.group_send(
            self.group_name,
            {
                "type": "group_message",
                "message": json.dumps({
                    "message_type": message_type,
                    "data": data,
                }),
            }
        )

    async def group_message(self, event):
        # Send the actual message to WebSocket
        await self.send(text_data=event["message"])

    async def receive(self, text_data):
        global event_started_9
        text_data_json = json.loads(text_data)
        message_type = text_data_json.get('type', None)

        if message_type == "start_event" and not event_started_9:
            event_started_9 = True
            await self.start_event()
        else:
            return

    async def start_event(self):
        await self.send_questions()

    @database_sync_to_async
    def get_random_questions(self):
        from fragen.models import Frage

        questions_queryset = Frage.objects.filter(kurs=9).order_by('?')[:10]
        questions_list = list(questions_queryset.values())
        return questions_list

    async def send_questions(self):
        questions = await self.get_random_questions()
        await self.send_message("questions", questions)