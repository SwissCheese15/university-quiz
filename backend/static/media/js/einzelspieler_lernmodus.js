/**
 * @file Logik für den Lernmodus (EINZELSPIELER)
 */

const quizContainer = document.getElementById("quiz-container");
const frageText = document.getElementById("frage-text");
const antwortenListe = document.getElementById("antworten");
const infoText = document.getElementById("info");
const naechsteFrageButton = document.getElementById("naechsteFrage");
const skipButton = document.getElementById("skip");
const kursid = getQueryVariable("kurs_id");
const MAX_BEANTWORTETE_FRAGEN = 10;
const korrektBeantwortetSpan = document.getElementById("korrekt-beantwortet");
const falschBeantwortetSpan = document.getElementById("falsch-beantwortet");
const uebersprungenSpan = document.getElementById("uebersprungen");

let korrektBeantwortet = 0;
let falschBeantwortet = 0;
let uebersprungen = 0;
let beantworteteFragen = []; 
let ausgewaehlteFrage = null; 

/**
 * Lädt eine neue Quizfrage und ihre Antwortmöglichkeiten. Filtert unbeantwortete Fragen basierend auf dem Kurs-ID und stellt eine zufällig ausgewählte Frage dar.
 * Verbirgt den "Nächste Frage"-Button und zeigt ihn nur an, wenn eine Antwort ausgewählt wird. Zeigt eine Nachricht an, wenn alle Fragen beantwortet wurden.
 */
async function loadFrage() {
	fragen = await fetchAllQuestions()
	let unbeantworteteFragen = fragen.filter((frage) => !beantworteteFragen.includes(frage.id));
	unbeantworteteFragen = unbeantworteteFragen.filter((frage) => frage.kurs == kursid);

	if (unbeantworteteFragen.length === 0) {
		frageText.textContent = "Alle Fragen aus diesem Kurs wurden beantwortet.";
		antwortenListe.innerHTML = "";
		infoText.textContent = "";
		naechsteFrageButton.style.display = "none"; 
		return;
	}

	const randomIndex = Math.floor(Math.random() * unbeantworteteFragen.length);
	ausgewaehlteFrage = unbeantworteteFragen[randomIndex];

	displayFrage(ausgewaehlteFrage);
}

/**
 * Mischt die Antwortmöglichkeiten einer Frage, um die Reihenfolge der Antworten bei jeder Anzeige zu variieren.
 *
 * @param {Array} antworten - Ein Array von Antwortmöglichkeiten.
 * @returns {Array} Ein neues Array mit zufällig angeordneten Antwortmöglichkeiten.
 */
function shuffleAntworten(antworten) {
	const shuffledAntworten = [...antworten];
	for (let i = shuffledAntworten.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[shuffledAntworten[i], shuffledAntworten[j]] = [shuffledAntworten[j], shuffledAntworten[i]];
	}
	return shuffledAntworten;
}

/**
 * Stellt die aktuell ausgewählte Quizfrage und ihre Antwortmöglichkeiten dar.
 * Setzt Daten-Attribute für Daumen-hoch und Daumen-runter Buttons zur Bewertung der Frage.
 *
 * @param {Object} frage - Ein Objekt, das die Details der Frage enthält, einschließlich Text und Antwortmöglichkeiten.
 */
function displayFrage(frage) {
    frageText.textContent = frage.fragetext;
    antwortenListe.innerHTML = "";
    infoText.textContent = "";

    const thumbsUpButton = document.querySelector(".thumbs-up .fa-thumbs-up");
    const thumbsDownButton = document.querySelector(".thumbs-up .fa-thumbs-down");

    thumbsUpButton.setAttribute("data-question-id", frage.id);
    thumbsDownButton.setAttribute("data-question-id", frage.id);

    const shuffledAntworten = shuffleAntworten([
        frage.antwort_richtig,
        frage.antwort_falsch_1,
        frage.antwort_falsch_2,
        frage.antwort_falsch_3,
    ]);

    shuffledAntworten.forEach((antwort) => {
        const antwortItem = document.createElement("li");
        antwortItem.textContent = antwort;
        antwortItem.addEventListener("click", () => antwortAuswaehlen(antwort, antwortItem));
        antwortenListe.appendChild(antwortItem);
    });
}

/**
 * Handhabt die Logik, die ausgeführt wird, wenn eine Antwort ausgewählt wird. Markiert die ausgewählte Antwort,
 * deaktiviert die Möglichkeit, weitere Antworten auszuwählen, und zeigt den "Nächste Frage"-Button an.
 *
 * @param {string} antwort - Der Text der ausgewählten Antwort.
 * @param {HTMLElement} antwortElement - Das DOM-Element der ausgewählten Antwort.
 */
function antwortAuswaehlen(antwort, antwortElement) {
    naechsteFrageButton.style.display = "block";
    skipButton.style.display = "none";

    antwortenListe.querySelectorAll("li").forEach((item) => {
        item.style.pointerEvents = "none";
        if (item.textContent === ausgewaehlteFrage.antwort_richtig) {
            item.classList.add("korrekt");
        } else {
            item.classList.add("falsch");
        }
        if (item.textContent === antwort) {
            item.classList.add("ausgewaehlt");
        }
    });

    if (antwort === ausgewaehlteFrage.antwort_richtig) {
		infoText.classList.add("korrekt");
        infoText.textContent += ausgewaehlteFrage.info_richtig;
        statAnswer(kursid, "increment-lernmodus-richtig")
        korrektBeantwortet++;
        infoText.innerHTML += ' <i class="fas fa-check"></i>';
    } else {
		infoText.classList.add("falsch");
		statAnswer(kursid, "increment-lernmodus-falsch")
        falschBeantwortet++;
        if (antwort === ausgewaehlteFrage.antwort_falsch_1) {
            infoText.textContent += ausgewaehlteFrage.info_falsch_1;
        } else if (antwort === ausgewaehlteFrage.antwort_falsch_2) {
            infoText.textContent += ausgewaehlteFrage.info_falsch_2;
        } else if (antwort === ausgewaehlteFrage.antwort_falsch_3) {
            infoText.textContent += ausgewaehlteFrage.info_falsch_3;
        }
        infoText.innerHTML += ' <i class="fas fa-times"></i>';
    }

    beantworteteFragen.push(ausgewaehlteFrage.id);
    if (beantworteteFragen.length > MAX_BEANTWORTETE_FRAGEN) {
        beantworteteFragen.shift();
    }

    korrektBeantwortetSpan.textContent = `${korrektBeantwortet}`;
    falschBeantwortetSpan.textContent = `${falschBeantwortet}`;
}


/**
 * Fügt dem "Überspringen"-Button einen Event Listener hinzu. Erhöht den Zähler für übersprungene Fragen, aktualisiert die Anzeige und lädt die nächste Frage.
 */
skip.addEventListener("click", () => {
	uebersprungen++;
	uebersprungenSpan.textContent = `${uebersprungen}`;
	
	antwortenListe.querySelectorAll("li").forEach((item) => {
		item.style.pointerEvents = "pointer";
	});

	infoText.textContent = "";
	infoText.classList.remove("korrekt", "falsch");
    enableThumbs()
	loadFrage();
});

/**
 * Fügt dem "Nächste Frage"-Button einen Event Listener hinzu. Zeigt den "Überspringen"-Button an, verbirgt den "Nächste Frage"-Button und lädt die nächste Frage.
 */
naechsteFrageButton.addEventListener("click", () => {
	skipButton.style.display = "block";
	naechsteFrageButton.style.display = "none"; 
	
	antwortenListe.querySelectorAll("li").forEach((item) => {
		item.style.pointerEvents = "pointer";
	});

	infoText.textContent = "";
	infoText.classList.remove("korrekt", "falsch");
    enableThumbs()
	loadFrage();
});


loadFrage();
