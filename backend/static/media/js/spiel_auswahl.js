/**
 * @file Logik für die Auswahl Einzelspieler oder Mehrspieler
 */

const modus = getQueryVariable("modus");
let msUntilStart = 0
let quizSocket

/**
 * Stellt die Ansicht basierend auf dem ausgewählten Spielmodus ein.
 * Der Modus wird über eine URL-Query-Variable ("modus") bestimmt.
 * Abhängig vom Wert dieser Variable wird entweder der Einzelspieler- oder der Mehrspieler-Modus angezeigt.
 * Sollte kein gültiger Modus übergeben werden, wird der Nutzer auf die Startseite umgeleitet.
 */
function setModusView() {
    const modus = getQueryVariable("modus");
    const einzelspielerDiv = document.getElementById('einzelspieler');
    const mehrspielerDiv = document.getElementById('mehrspieler');

    if (modus === "Einzelspieler") {
        einzelspielerDiv.style.display = 'flex';
        mehrspielerDiv.style.display = 'none';
    } else if (modus === "Live-Event") {
        einzelspielerDiv.style.display = 'none';
        mehrspielerDiv.style.display = 'block';
    } else {
        window.location.href = 'index.html';
    }
}

/**
 * Initialisiert und aktualisiert den Countdown für das nächste Mehrspieler-Event.
 * Diese Funktion berechnet die verbleibende Zeit bis zum Beginn und Ende des Events und aktualisiert
 * entsprechend die Anzeige sowie den Status des Teilnahmebuttons.
 * Der Countdown wird jede Sekunde aktualisiert.
 */

let startEventMessageSend = false

function countdownTimer() {
	const now = new Date();
	let eventStartTime = new Date();
	let eventEndTime = new Date();

	if (now.getHours() > 18 || (now.getHours() === 18 && now.getMinutes() >= 10 && now.getSeconds() >= 10)) {
		eventStartTime.setDate(now.getDate() + 1);
		eventEndTime.setDate(now.getDate() + 1);
	}

	eventStartTime.setHours(18, 0, 0, 0);
	eventEndTime = new Date(eventStartTime.getTime() + 5 * 60000);

	const timeToStart = eventStartTime - now;
	const timeToEnd = eventEndTime - now;

	const teilnahmeButton = document.getElementById('teilnahmeButton');
	const mehrspielerText = document.querySelector('.mehrspieler-text');

	const isSameDay = now.toDateString() === eventStartTime.toDateString();

	if (now < eventStartTime) {
		updateCounter(timeToStart, "");
		teilnahmeButton.disabled = true;
		teilnahmeButton.style.cursor = 'not-allowed';
		mehrspielerText.textContent = isSameDay
		  ? `Nächstes Event beginnt heute um ${eventStartTime.toLocaleTimeString()}`
		  : `Nächstes Event beginnt am ${eventStartTime.toLocaleDateString()} um ${eventStartTime.toLocaleTimeString()}`;
	} else if (now < eventEndTime) {
		msUntilStart = timeToEnd
		updateCounter(timeToEnd, "Teilnahmeschluss<br>");
		teilnahmeButton.disabled = false;
		teilnahmeButton.style.cursor = 'pointer';
		if (!mehrspielerText.getAttribute("inLobby")) {
			mehrspielerText.textContent = "Das Event läuft! Jetzt teilnehmen!";
		}
	} else {
		if (!startEventMessageSend && quizSocket) {
			startEventMessageSend = true
			quizSocket.send(JSON.stringify({
				type: "start_event"
			}));
		}
		teilnahmeButton.disabled = true;
		teilnahmeButton.style.cursor = 'not-allowed';
		if (!mehrspielerText.getAttribute("inLobby")) {
			mehrspielerText.textContent = isSameDay
			  ? `Nächstes Event beginnt heute um ${eventStartTime.toLocaleTimeString()}`
			  : `Nächstes Event beginnt am ${eventStartTime.toLocaleDateString()} um ${eventStartTime.toLocaleTimeString()}`;
		}
	}

	setTimeout(countdownTimer, 1000);
}

function joinMultiplayer() {
	const teilnahmeButton = document.getElementById('teilnahmeButton');
	const mehrspielerText = document.querySelector('.mehrspieler-text');
	teilnahmeButton.addEventListener("click", () => {
		teilnahmeButton.style.display = "none"
		mehrspielerText.setAttribute("inLobby", true)
		startWebsocketLogic()
	})
}

function startWebsocketLogic() {
	const kursid = getQueryVariable("kurs_id");
	const accessToken = localStorage.getItem('access');

	quizSocket = new WebSocket(`ws://167.172.184.62:8001/ws-quiz-show/${kursid}/?token=${accessToken}`);

    quizSocket.onopen = function(event) {
		authenticateToWebsocket()
    };

	function authenticateToWebsocket() {
		quizSocket.send(JSON.stringify({ 'authorization': `Bearer ${accessToken}` }));
	}

    quizSocket.onmessage = function(event) {

		const message = JSON.parse(event.data)

		if (message.message_type === "user_list") receiveUserList(message)
		else if (message.message_type === "questions") {
			localStorage.setItem('questions', JSON.stringify(message.data))
			multiplayerQuizLogic()
		}
    };

	function receiveUserList(message) {
		const mehrspielerText = document.querySelector('.mehrspieler-text');
		mehrspielerText.textContent = ""
		const userListTitle = document.createElement("h3")
		userListTitle.innerText = "Teilnehmer:"
		mehrspielerText.appendChild(userListTitle)
		for (let i = 0; i < message.data.length; i++) {
			const userElement = document.createElement("p")
			userElement.textContent = message.data[i]
			mehrspielerText.appendChild(userElement)
		}

		function mountUserListOnQuizSite() {
			const mehrspielerText = document.querySelector('.mehrspieler-text')
			const teilnehmerListe = document.getElementById("teilnehmer-liste")
			teilnehmerListe.appendChild(mehrspielerText)
			teilnehmerListe.style.marginRight = "40px"
		}
		const liveEventSite = document.getElementById("modus-liveevent")
		if (liveEventSite.style.display === "flex") mountUserListOnQuizSite()
	}
}



/**
 * Aktualisiert den Countdown-Timer mit der verbleibenden Zeit bis zum Event.
 * @param {number} time - Die verbleibende Zeit in Millisekunden.
 * @param {string} prefix - Ein optionaler Text, der vor der verbleibenden Zeit angezeigt wird.
 */
function updateCounter(time, prefix) {
	const hours = Math.floor(time / (1000 * 60 * 60));
	const minutes = Math.floor((time / (1000 * 60)) % 60);
	const seconds = Math.floor((time / 1000) % 60);
	let formattedTime = '';

	if (hours > 0) {
	  formattedTime += `${hours.toString().padStart(2, '0')}h `;
	}
	if (minutes > 0 || hours > 0) {
	  formattedTime += `${minutes.toString().padStart(2, '0')}m `;
	}
	formattedTime += `${seconds.toString().padStart(2, '0')}s`;



	const mehrspielerCounter = document.getElementById('mehrspielerCounter');
	mehrspielerCounter.innerHTML = `${prefix} ${formattedTime}`;
}

/**
 * Initialisiert die Benutzeroberfläche und Funktionalitäten vin QUIZZIU, sobald das DOM vollständig geladen ist.
 * - Aktualisiert die Titelbox und den Modus basierend auf URL-Parametern.
 * - Initialisiert eine Countdown-Timer-Funktion.
 * - Ändert den Dokumenttitel basierend auf dem gewählten Modus (wenn vorhanden).
 * - Ermöglicht die Navigation zwischen verschiedenen Spielmodi (Lernmodus und Quizshow) durch Klick auf die entsprechenden Modus-Boxen.
 * - Zeigt detaillierte Beschreibungen für jeden Modus beim Überfahren mit der Maus an und kehrt beim Verlassen zum Standardtext zurück.
 *
 * Der Code setzt voraus, dass Funktionen wie `updateTitleBox`, `updateModus`, `setModusView` und `countdownTimer` bereits definiert sind
 * und dass bestimmte Elemente im HTML-Dokument vorhanden sind (z.B. Modus-Boxen).
 */
document.addEventListener('DOMContentLoaded', function() {
	updateTitleBox();
	updateModus();
	setModusView();
	countdownTimer();
	joinMultiplayer()
	if (modus) {
		document.title = `QUIZZIU | ${modus}`;
	}

    const lernmodusText = "Der Lernmodus ermöglicht es dir, in deinem eigenen Tempo zu lernen und das Wissen im Endlosmodus zu vertiefen.";
    const quizshowText = "Die Quizshow bietet dir die Möglichkeit, dein Wissen in einem Wettbewerbsumfeld unter Beweis zu stellen.";

	var modusContainer = document.getElementById('einzelspieler');
	var modusBoxes = modusContainer.getElementsByClassName('modus-box');
	const kursid = getQueryVariable("kurs_id");
	for (var i = 0; i < modusBoxes.length; i++) {
		modusBoxes[i].addEventListener('click', function() {
		var modusText = this.querySelector('.modus-text').textContent;

		if (modusText === lernmodusText) {
			const weiterleitungsURL = `einzelspieler.html?kurs_id=${kursid}&modus=Lernmodus`;
			window.location.href = weiterleitungsURL;
		} else if (modusText === quizshowText) {
			const weiterleitungsURL = `einzelspieler.html?kurs_id=${kursid}&modus=Quizshow`;
			window.location.href = weiterleitungsURL;
		}
		});
	}
	const lernmodusBox = document.querySelector('.modus-box:nth-child(1)');
    const quizshowBox = document.querySelector('.modus-box:nth-child(2)');

    lernmodusBox.addEventListener('mouseenter', function() {
        this.querySelector('.modus-text').textContent = lernmodusText;
    });
    lernmodusBox.addEventListener('mouseleave', function() {
        this.querySelector('.modus-text').textContent = 'Lernmodus';
    });

    quizshowBox.addEventListener('mouseenter', function() {
        this.querySelector('.modus-text').textContent = quizshowText;
    });
    quizshowBox.addEventListener('mouseleave', function() {
        this.querySelector('.modus-text').textContent = 'Quizshow';
    });
});

// Multiplayer Script included here to tackle websocket state issue
let rightAnswerGiven = false
let wrongAnswerGiven = false

function multiplayerQuizLogic() {
	const liveEventSite = document.getElementById("modus-liveevent")
	const mehrspielerLobby = document.getElementById("mehrspieler")
	liveEventSite.style.display = "flex"
	mehrspielerLobby.style.display = "none"
	const kursId = getQueryVariable("kurs_id");
	let currentQuestionIndex = 0;
	let currentPoints = 0;
	let selectedQuestions = JSON.parse(localStorage.getItem('questions'));
	let safetyNetActive = false;

	loadQuestion(selectedQuestions[currentQuestionIndex]);

	async function statAnswer(kursId, mode) {
    const accessToken = localStorage.getItem('access');
    const url = `http://167.172.184.62/api/v1/beantworten/${kursId}/${mode}/`;

    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessToken}`
            }
        });

        if (!response.ok) {
            throw new Error(`Anfrage fehlgeschlagen: ${response.status}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.error("Fehler beim Senden der Antwort:", error);
    }
}

function mountUserListOnQuizSite() {
	const mehrspielerText = document.querySelector('.mehrspieler-text')
	const teilnehmerListe = document.getElementById("teilnehmer-liste")
	teilnehmerListe.appendChild(mehrspielerText)
	teilnehmerListe.style.marginRight = "40px"
}
mountUserListOnQuizSite()

function loadQuestion(question) {
		rightAnswerGiven = false
		document.getElementById('counter').style.display = 'none';
		document.getElementById('question-text').innerHTML = `<p>${question.fragetext}</p>`;
		let answers = [question.antwort_richtig, question.antwort_falsch_1, question.antwort_falsch_2, question.antwort_falsch_3];
		document.getElementById('info_quizshow').style.display = 'none';
		shuffleArray(answers);
		const answerList = document.getElementById('answer-list');
		answerList.innerHTML = '';
		answers.forEach(answer => {
			let li = document.createElement('li');
			li.textContent = answer;
			li.onclick = () => handleAnswerSelection(answer);
			answerList.appendChild(li);
		});

		let remainingTime = 30

		// Time limit per question
		const timeLimit = setInterval(() => {
			if (remainingTime > 0) {
				displayRemainingTime(remainingTime)
				remainingTime -= 1
			} else if (rightAnswerGiven) {
				enableClickInteractions()
				clearInterval(timeLimit);
				loadNextQuestion();
				updatePointsDisplay();
			} else {
				quizSocket.close(1000, "no_answer_given");
				clearInterval(timeLimit)
				if (!wrongAnswerGiven) {
					document.getElementById('info_quizshow').style.display = 'block';
					document.getElementById('info_quizshow').style.backgroundColor = 'red';
					document.getElementById('info_quizshow').innerHTML = `
						<p>Sorry, du hast nicht rechtzeitig geantwortet.</p>
						<p>Du hast es bis zu level ${currentPoints + 1} geschafft.</p>
						<button id="playLaterButton">Verlassen</button>
					`;
					document.getElementById('quiz_quizshow').style.display = 'none';
					document.getElementById('playLaterButton').addEventListener('click', backToPreviousPage);
				}
			}
		}, 1000);
	}

	function shuffleArray(array) {
		for (let i = array.length - 1; i > 0; i--) {
			let j = Math.floor(Math.random() * (i + 1));
			[array[i], array[j]] = [array[j], array[i]];
		}
	}

	function displayRemainingTime(time) {
		document.getElementById("remaining-time").innerHTML = `Verbleibende Zeit: ${time}`;
	}

	/**
     * Steuern die Klickinteraktionen auf der Seite. `disableClickInteractions` deaktiviert Klicks auf alle interaktiven Elemente,
     */
	function disableClickInteractions() {
		document.querySelectorAll('button, a, #answer-list li').forEach(element => {
			element.style.pointerEvents = 'none';
		});
	}

    /**
     * Steuern die Klickinteraktionen auf der Seite. `disableClickInteractions` aktiviert Klicks auf alle interaktiven Elemente,
     */
	function enableClickInteractions() {
		document.querySelectorAll('button, a, #answer-list li').forEach(element => {
			element.style.pointerEvents = 'all';
		});
	}

	function handleAnswerSelection(selectedAnswer) {
		let question = selectedQuestions[currentQuestionIndex];
		if (selectedAnswer === question.antwort_richtig) {
			disableClickInteractions()
			rightAnswerGiven = true
			document.getElementById('info_quizshow').style.display = 'block';
			document.getElementById('info_quizshow').style.backgroundColor = 'green';
			document.getElementById('info_quizshow').innerHTML = `<p>${question.info_richtig}</p>`;
			currentPoints++;
			safetyNetActive = false;
			statAnswer(kursId, "increment-liveevent-richtig")
		} else {
			if (safetyNetActive) {
				safetyNetActive = false;
				document.getElementById('info_quizshow').style.display = 'block';
				document.getElementById('info_quizshow').style.backgroundColor = '#4e8ead';
				document.getElementById('info_quizshow').innerHTML = `<p>[SICHERHEITSNETZ] Falsche Antwort. Versuchen Sie es noch einmal.</p>`;
				const answerListItems = document.querySelectorAll('#answer-list li');
				answerListItems.forEach(item => {
					item.style.pointerEvents = 'all';
					item.onclick = () => handleAnswerSelection(item.textContent);
				});
		} else {
			wrongAnswerGiven = true
			quizSocket.close(1000, "no_answer_given");
			document.getElementById('info_quizshow').style.display = 'block';
			document.getElementById('info_quizshow').style.backgroundColor = 'red';
			document.getElementById('info_quizshow').innerHTML = `
				<p>${getWrongInfo(selectedAnswer, question)}</p>
				<p>Du hast es bis zu level ${currentPoints + 1} geschafft.</p>
				<button id="playLaterButton">Verlassen</button>
			`;
			document.getElementById('quiz_quizshow').style.display = 'none';
			document.getElementById('playLaterButton').addEventListener('click', backToPreviousPage);
			statAnswer(kursId, "increment-liveevent-falsch")
		}
		}
	}

	function getWrongInfo(selectedAnswer, question) {
		if (selectedAnswer === question.antwort_falsch_1) return question.info_falsch_1;
		if (selectedAnswer === question.antwort_falsch_2) return question.info_falsch_2;
		return question.info_falsch_3;
	}

	function loadNextQuestion() {
		if (currentQuestionIndex < selectedQuestions.length - 1) {
			currentQuestionIndex++;
			loadQuestion(selectedQuestions[currentQuestionIndex]);
			document.getElementById('question-area').style.display = 'block';
		} else {
			endQuiz();
		}
	}

	function endQuiz() {
		document.getElementById('quiz_quizshow').style.display = 'none';
		document.getElementById('info_quizshow').style.display = 'block';
		document.getElementById('info_quizshow').style.backgroundColor = 'green';
		document.getElementById('info_quizshow').innerHTML = `<p>Herzlichen Glückwunsch! Du hast das Quiz abgeschlossen.</p>`;

		generateConfetti();
	}

	function generateConfetti() {
		let confettiWrapper = document.getElementById('confetti-wrapper');
		if (!confettiWrapper) {
			confettiWrapper = document.createElement('div');
			confettiWrapper.id = 'confetti-wrapper';
			document.body.appendChild(confettiWrapper);
		}

		for (let i = 0; i < 100; i++) {
			let confetti = document.createElement('div');
			confetti.className = 'confetti';
			confetti.style.left = `${Math.random() * 100}%`;
			confetti.style.backgroundColor = getRandomColor();
			confetti.style.transform = `rotate(${Math.random() * 360}deg)`;
			confettiWrapper.appendChild(confetti);
		}
	}

	function getRandomColor() {
		const colors = ['#FF0', '#F00', '#0F0', '#00F', '#F0F', '#0FF'];
		return colors[Math.floor(Math.random() * colors.length)];
	}

	function updatePointsDisplay() {
		const pointsItems = document.querySelectorAll('.points-item');
		pointsItems.forEach(item => {
			item.classList.remove('selected');
			item.style.backgroundColor = '#F3F3F7';
		});

		for (let i = 0; i < currentPoints; i++) {
			pointsItems[9 - i].style.backgroundColor = '#4CAF50';
		}

		if (currentPoints < 10) {
			pointsItems[9 - currentPoints].classList.add('selected');
			pointsItems[9 - currentPoints].style.backgroundColor = '';
		}
	}


	function getWrongAnswers(question) {
		return [question.antwort_falsch_1, question.antwort_falsch_2, question.antwort_falsch_3];
	}

	function removeRandomWrongAnswers(wrongAnswers, count) {
		shuffleArray(wrongAnswers);
		let removedCount = 0;
		const answerElements = document.querySelectorAll("#answer-list li");

		answerElements.forEach(answerElement => {
			if (removedCount < count && wrongAnswers.includes(answerElement.textContent)) {
				answerElement.remove();
				removedCount++;
			}
		});
	}

	function displayHint(question) {
		let wrongInfos =  [question.info_falsch_1, question.info_falsch_2, question.info_falsch_3];
		document.getElementById('info_quizshow').style.backgroundColor = '#4e8ead';
		document.getElementById('info_quizshow').style.display = 'block';
		document.getElementById('info_quizshow').textContent = "[TIPP] " + wrongInfos[Math.floor(Math.random() * wrongInfos.length)];
	}

	function backToPreviousPage() {
		window.history.back();
	}

	function disableClickInteractions() {
		document.querySelectorAll('button, a, #answer-list li').forEach(element => {
			element.style.pointerEvents = 'none';
		});
	}

	function enableClickInteractions() {
		document.querySelectorAll('button, a, #answer-list li').forEach(element => {
			element.style.pointerEvents = 'all';
		});
	}


	document.querySelector('.fifty-fifty').addEventListener('click', function() {
		let wrongAnswers = getWrongAnswers(selectedQuestions[currentQuestionIndex]);
		removeRandomWrongAnswers(wrongAnswers, 2);
		this.classList.add('disabled');
		this.removeEventListener('click', arguments.callee);
	});

	document.querySelector('.hint').addEventListener('click', function() {
		displayHint(selectedQuestions[currentQuestionIndex]);
		displayHint(selectedQuestions[currentQuestionIndex]);
		this.classList.add('disabled');
		this.removeEventListener('click', arguments.callee);
	});

	document.querySelector('.safety-net').addEventListener('click', function() {
		safetyNetActive = true;
		this.classList.add('disabled');
		this.removeEventListener('click', arguments.callee);
	});
}
