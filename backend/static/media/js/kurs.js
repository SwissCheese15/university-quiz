/**
 * @file Logik für die Option im Kurs
 */

/**
 * Lädt die Kursdaten asynchron und aktualisiert die Webseite mit Kursinformationen.
 * Setzt den Dokumenttitel und die Inhalte der Titelbox basierend auf dem abgerufenen Kurs.
 * @async
 * @function loadKursData
 */
async function loadKursData() {
    const kursId = getQueryVariable('kurs_id');
    const kurse = await fetchAllKurse();
    const kurs = kurse.find(k => k.id.toString() === kursId);

    if (kurs) {
        document.title = `QUIZZIU | ${kurs.name}`;
        document.querySelector('.title-box h1').textContent = kurs.name;
        document.querySelector('.title-box p').textContent = kurs.code;
    }
}

loadKursData().catch(console.error);

/**
 * Leitet den Benutzer zu einer spezifischen URL weiter, ergänzt um die Kurs-ID als Query-Parameter.
 * @function redirectToId
 * @param {string} url - Die Basis-URL, zu der weitergeleitet wird.
 */
function redirectToId(url) {
    const kursId = getQueryVariable('kurs_id');
    if (kursId) { 
        window.location.href = url + '?kurs_id=' + kursId;
    } else {
        window.location.href = url;
    }
}

/**
 * Leitet den Benutzer zu einer spezifischen URL weiter, ergänzt um Kurs-ID und Modus als Query-Parameter.
 * Unterstützt die Modus-Auswahl für Einzelspieler oder Mehrspieler.
 * @function redirectToIdModus
 * @param {string} url - Die Basis-URL, zu der weitergeleitet wird.
 * @param {number} modus - Der Modus, 1 für Einzelspieler, 2 für Mehrspieler.
 */
function redirectToIdModus(url, modus) {
    const kursId = getQueryVariable('kurs_id');
    let zusatzParam = '';
    if (modus === 1) {
        zusatzParam = '&modus=Einzelspieler';
    } else if (modus === 2) {
        zusatzParam = '&modus=Live-Event';
    }

    if (kursId) {
        window.location.href = `${url}?kurs_id=${kursId}${zusatzParam}`;
    } else {
        window.location.href = url;
    }
}

/**
 * Fügt jedem Element mit der Klasse `.grid-item` Event-Listener für `mouseenter` und `mouseleave` hinzu.
 * Beim Betreten eines Elements wird die Funktion `updateProgress` aufgerufen, um den Fortschrittsbalken zu aktualisieren,
 * und beim Verlassen wird die Funktion `resetProgress` aufgerufen, um den Fortschrittsbalken zurückzusetzen.
 */
document.querySelectorAll('.grid-item').forEach(item => {
    item.addEventListener('mouseenter', () => updateProgress(item));
    item.addEventListener('mouseleave', () => resetProgress(item));
});

/**
 * Aktualisiert den Fortschrittsbalken eines Elementes basierend auf Benutzerstatistiken.
 * Die Funktion holt asynchron die benötigten Daten, berechnet den Fortschrittsprozentsatz und passt die Darstellung des Fortschrittsbalkens entsprechend an.
 *
 * @param {HTMLElement} element - Das Element, dessen Fortschrittsbalken aktualisiert wird.
 * Es wird erwartet, dass dieses Element ein Kind-Element mit der Klasse `.progress-ring__circle` enthält, welches den Fortschrittsbalken darstellt.
 */
async function updateProgress(element) {
    const circle = element.querySelector('.progress-ring__circle');
    if (circle) {
        // Holt die Kurs-ID aus der URL und die Benutzer-ID aus dem lokalen Speicher.
        const kursId = getQueryVariable('kurs_id');
        const userId = localStorage.getItem('accountId');

        // Lädt alle Fragen und Benutzerstatistiken asynchron.
        fragen = await fetchAllQuestions();
        const stats = await fetchStats();
        const userStats = stats.find(stat => stat.kurs == kursId && stat.user == userId);
        if (userStats) {
            // Berechnet den Gesamtfortschritt und die Genauigkeit der Antworten.
            totalFragen = userStats.lernmodus_richtig + userStats.quizshow_richtig + userStats.liveevent_richtig;
            totalFalsch = userStats.lernmodus_falsch + userStats.quizshow_falsch + userStats.liveevent_falsch;
            prozentRichtig = totalFragen / (totalFragen + totalFalsch) * 100;

        } else {
            prozentRichtig = 0;
        }
        const prozentWert = document.getElementById('prozentWert');
        if (prozentWert) {
                prozentWert.textContent = `${prozentRichtig.toFixed(1)}%`;
                setTimeout(() => {
                    prozentWert.classList.add('visible');
                }, 100);
            }

            const circumference = circle.getTotalLength();
            const offset = circumference - (prozentRichtig / 100) * circumference;
            circle.style.strokeDasharray = `${circumference} ${circumference}`;
            circle.style.strokeDashoffset = offset;

            // Dynamische Farbänderung für den füllenden Teil
            circle.style.transition = 'stroke-dashoffset 2s ease, stroke 2s ease';
            circle.style.stroke = "#4e8ead";
    }
}

/**
 * Setzt den Fortschrittsbalken eines Elements auf den Anfangszustand zurück.
 * Dies beinhaltet das Zurücksetzen der `strokeDashoffset`-Eigenschaft des Kreises, um den Fortschritt visuell zu entfernen,
 * und das Verbergen des Prozentwertes.
 *
 * @param {HTMLElement} element - Das Element, dessen Fortschrittsbalken zurückgesetzt wird.
 * Wie bei `updateProgress` wird erwartet, dass dieses Element ein Kind-Element mit der Klasse `.progress-ring__circle` enthält.
 */
function resetProgress(element) {
    const circle = element.querySelector('.progress-ring__circle');
    if (circle) {
        const circumference = circle.getTotalLength();
        circle.style.strokeDasharray = `${circumference} ${circumference}`;
        circle.style.strokeDashoffset = circumference;
        // Aktualisiert den Wert im SVG
        const prozentWert = document.getElementById('prozentWert');
        if (prozentWert) {
            setTimeout(() => {
                prozentWert.classList.remove('visible');
            }, 100);
        }
        circle.style.transition = 'stroke-dashoffset 2s ease, stroke 2s ease';
        circle.style.stroke = "#4e8ead";
    }
}
