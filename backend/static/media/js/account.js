/**
 * @file Logik für Login und Profil
 */

/**
 * default ID um zu Zeigen - nicht angemeldet
 */
let accountId = 0;

/**
 * Überprüft, ob ein Benutzer eingeloggt ist, indem das Vorhandensein eines Zugriffstokens im lokalen Speicher geprüft wird.
 * Leitet Benutzer basierend auf ihrem Login-Status und der aktuellen Seite um. Verifiziert das Zugriffstoken mit dem Server.
 * Leitet bei Erfolg auf die Hauptseite um, wenn der Benutzer bereits eingeloggt ist und versucht, die Login-Seite aufzurufen.
 */
async function checkLogin() {
    const accessToken = localStorage.getItem('access');
    const currentPage = window.location.pathname.split('/').pop();

    if (accessToken && (currentPage === 'ueber-uns.html' || currentPage === 'link_tree.html')) {
        const loginLink = document.querySelector('a[href="login.html"]');
        const kontaktLink = document.querySelector('a[href="https://www.iu.de/hochschule/studierendensekretariat/"]');
        if (loginLink) loginLink.style.display = 'None';
        if (kontaktLink) kontaktLink.style.display = 'None';
        return;
    }  else if (!accessToken && (currentPage === 'ueber-uns.html' || currentPage === 'link_tree.html')) {
        return;
    }

    if (!accessToken) {
        redirectToLogin(currentPage);
        return;
    }

    try {
        const response = await fetch('http://167.172.184.62/api/v1/auth/token/verify/', {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ token: accessToken })
        });

        if (!response.ok) {
            throw new Error('Token verification failed');
        }

        if (currentPage === 'login.html') {
            window.location.href = 'index.html';
        }
    } catch (error) {
        console.error('Error:', error);
        redirectToLogin(currentPage);
    }
}

/**
 * Leitet den Benutzer zur Login-Seite um, es sei denn, er befindet sich bereits dort.
 *
 * @param {string} currentPage - Der Name der aktuellen Seite, um unnötige Umleitungen zu vermeiden.
 */
function redirectToLogin(currentPage) {
    if (currentPage !== 'login.html') {
        window.location.href = 'login.html';
    }
}

/**
 * Zeigt den Benutzernamen an einer vorgesehenen Stelle in der Benutzeroberfläche an.
 * Holt den Benutzernamen aus dem lokalen Speicher und setzt dessen Wert in das entsprechende DOM-Element.
 */
async function displayUsername() {
    const accountName = localStorage.getItem('accountName');

    try {
        if (accountName) {
            document.getElementById('usernameDisplay').textContent = accountName;
        }
    } catch (error) {

    }
}

/**
 * Entfernt Benutzeridentifikation und Zugriffstoken aus dem lokalen Speicher und leitet den Benutzer zur Login-Seite um.
 */
function logout() {
	localStorage.removeItem('accountId');
	localStorage.removeItem('access');
	window.location.href = 'login.html';
}

/**
 * Lädt die Profildaten des Benutzers basierend auf der im lokalen Speicher gespeicherten Benutzer-ID.
 * Aktualisiert die Eingabefelder im Profilbereich der Webanwendung mit den geladenen Daten.
 */
async function getProfileDataById() {
    const accountId = localStorage.getItem('accountId');
    
    try {
        const user = await fetchUserData(accountId);
        if (user) {
            document.getElementById('emailField').value = user.email;
            document.getElementById('passwordField').value = ""; 
            document.getElementById('usernameField').value = user.username;

            let profilePicFilename = user.photo ? user.photo : "PHOTO_ONE";
            document.getElementById('currentProfilePicture').src = `media/img/profilbild/${profilePicFilename}.png`;
        }
    } catch (error) {
        console.error('Fehler beim Abrufen der Profildaten:', error);
    }
}

/**
 * Zeigt Fehlermeldungen neben Formularfeldern an , basierend auf der Validierung der Eingaben.
 *
 * @param {string} fieldId - Die ID des Formularfeldes, für das die Fehlermeldung angezeigt oder entfernt wird.
 * @param {string} [message] - Die anzuzeigende Fehlermeldung. Wird nur für `showFieldErrorMessage` benötigt.
 */
function showFieldErrorMessage(fieldId, message) {
    const field = document.getElementById(fieldId);
    const errorSpanId = fieldId + 'Error';
    let errorSpan = document.getElementById(errorSpanId);

    if (!errorSpan) {
        errorSpan = document.createElement('span');
        errorSpan.id = errorSpanId;
        errorSpan.classList.add('error-message');
        field.parentNode.insertBefore(errorSpan, field.nextSibling);
    }

    errorSpan.textContent = message;
}

/**
 * Entfernt Fehlermeldungen neben Formularfeldern, basierend auf der Validierung der Eingaben.
 *
 * @param {string} fieldId - Die ID des Formularfeldes, für das die Fehlermeldung angezeigt oder entfernt wird.
 */
function removeFieldErrorMessage(fieldId) {
    const errorSpan = document.getElementById(fieldId + 'Error');
    if (errorSpan) {
        errorSpan.remove();
    }
}

/**
 * Validiert das Eingabefeld basierend auf seiner ID und dem Wert.
 * Zeigt Fehlermeldungen für ungültige Eingaben an und entfernt sie für gültige Eingaben.
 *
 * @param {string} fieldId - Die ID des zu validierenden Feldes.
 * @param {string} value - Der Wert des Feldes, der validiert werden soll.
 * @returns {boolean} Gibt `true` zurück, wenn die Validierung erfolgreich war, sonst `false`.
 */
function validateField(fieldId, value) {
    let isValid = false;
    let errorMessage = '';

    switch (fieldId) {
        case 'emailField':
            isValid = validateEmail(value);
            errorMessage = 'Ungültige E-Mail-Adresse.';
            break;
        case 'passwordField':
            isValid = validatePassword(value);
            errorMessage = 'Ungültiges Passwort. Das Passwort muss mindestens 8 Zeichen lang sein und mindestens einen Buchstaben und eine Zahl enthalten.';
            break;
        case 'usernameField':
            isValid = validateUsername(value);
            errorMessage = 'Ungültiger Benutzername. Der Benutzername muss zwischen 5 und 25 Zeichen lang sein und darf keine aufeinanderfolgenden Leerzeichen enthalten.';
            break;
        default:
            isValid = true;
    }

    if (!isValid) {
        showFieldErrorMessage(fieldId, errorMessage);
    } else {
        removeFieldErrorMessage(fieldId);
    }

    return isValid;
}

/**
 * Wechselt den Bearbeitungsmodus für Profilfelder, ermöglicht das Bearbeiten von Werten und deren Speicherung.
 * Wechselt zwischen Bearbeiten- und Speichern-Icons und aktiviert bzw. deaktiviert das entsprechende Eingabefeld.
 *
 * @param {string} fieldId - Die ID des Feldes, das bearbeitet werden soll.
 */
function toggleEdit(fieldId) {
    var field = document.getElementById(fieldId);
    var editIcon = field.closest('.input-group').querySelector('.fa-pencil-alt, .fa-save');
    var editText = field.closest('.input-group').querySelector('.edit');

    if (field) {
        field.disabled = !field.disabled;
        if (!field.disabled) {
            field.focus();
            editIcon.classList.replace('fa-pencil-alt', 'fa-save');
            editText.textContent = "Save";
            editText.classList.remove('edit-text');
            editText.classList.add('save-text');
        } else {
            if (!validateField(fieldId, field.value)) {
                field.disabled = false;
            } else {
                editIcon.classList.replace('fa-save', 'fa-pencil-alt');
                editText.textContent = "Edit";
                editText.classList.add('edit-text');
                editText.classList.remove('save-text');
                showMessage("Erfolgreich gespeichert!", 1)
                saveProfileData(fieldId, field.value);
            }
        }
    }
}

/**
 * Speichert die aktualisierten Benutzerprofildaten, indem eine PATCH-Anfrage an den Server gesendet wird.
 * Verwendet die Benutzer-ID und das Zugriffstoken aus dem lokalen Speicher für die Authentifizierung.
 *
 * @param {string} fieldId - Die ID des Feldes, dessen Wert gespeichert wird.
 * @param {string} value - Der neue Wert des Feldes.
 */
async function saveProfileData(fieldId, value) {
    const accountId = localStorage.getItem('accountId');
    const accessToken = localStorage.getItem('access'); 
    const url = `http://167.172.184.62/api/v1/user/${accountId}/`;

    let data = {};
    if (fieldId === 'emailField') {
        data.email = value;
    } else if (fieldId === 'passwordField') {
        data.password = value;
    } else if (fieldId === 'usernameField') {
        data.username = value;
    } else if (fieldId === 'pic') {
        data.photo = value;
    }

    try {
        const response = await fetch(url, {
            method: 'PATCH',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            throw new Error(`Fehler beim Aktualisieren der Profildaten: ${response.status}`);
        }
        if (fieldId === 'usernameField') {
            localStorage.setItem('accountName', value);
            document.getElementById('usernameDisplay').textContent = value;
        }
        if (fieldId === 'pic') {
            localStorage.setItem('accountPhoto', value);
            document.getElementById('profilePicture').src = `media/img/profilbild/${value}.png`;
        }

    } catch (error) {
        console.error('Fehler:', error);
    }
}

const availablePictures = ["PHOTO_ONE", "PHOTO_TWO", "PHOTO_THREE", "PHOTO_FOUR", "PHOTO_FIVE", "PHOTO_SIX"];

let selectedPicture = "";

function openModal() {
  const modal = document.getElementById('profilePictureModal');
  const choicesContainer = document.getElementById('profilePictureChoices');
  choicesContainer.innerHTML = '';

  availablePictures.forEach(picture => {
    const img = document.createElement('img');
    img.src = `media/img/profilbild/${picture}.png`;
    img.alt = "Profilbild";
    img.onclick = function() {
      document.querySelectorAll('.profile-picture-choices img').forEach(i => i.classList.remove('selected'));
      img.classList.add('selected');
      selectedPicture = picture;
    };

    choicesContainer.appendChild(img);
  });

  modal.style.display = "block";
}


function changeProfilePicture(pictureName) {
  document.getElementById('currentProfilePicture').src = `media/img/profilbild/${pictureName}.png`;
  document.getElementById('profilePictureModal').style.display = "none";
  saveProfileData("pic", pictureName)
  selectedPicture = "";
}


window.onclick = function(event) {
  const modal = document.getElementById('profilePictureModal');
  if (event.target == modal) {
    modal.style.display = "none";
  }
}


/**
 * Ein Event Listener, der auf das vollständige Laden des Dokuments wartet, um Login-Status zu überprüfen,
 * den Benutzernamen anzuzeigen und gegebenenfalls Profildaten zu laden.
 */
document.addEventListener('DOMContentLoaded', function() {    const isDarkMode = getCookie('dark') === 'true';
    document.getElementById('darkModeStylesheet').disabled = !isDarkMode;

    checkLogin();
    displayUsername();


    try {
        const profilePictureSrc = localStorage.getItem('accountPhoto');
        const profilePictureElement = document.getElementById('profilePicture');
        if (profilePictureSrc === "null" || profilePictureSrc === null) {
            profilePictureElement.src = 'media/img/profilbild/PHOTO_ONE.png';
        } else {
            profilePictureElement.src = `media/img/profilbild/${profilePictureSrc}.png`;
        }
    } catch (error) {

    }

    const changeButton = document.getElementById('changeProfilePictureBtn');
    const modal = document.getElementById('profilePictureModal');
    const closeButton = document.getElementById('closeModalButton');


    if (window.location.pathname.split('/').pop() === 'profil.html') {
        getProfileDataById();

        changeButton.addEventListener('click', function() {
            if (selectedPicture) {
                changeProfilePicture(selectedPicture);
            }
        });

        closeButton.addEventListener('click', function() {
            modal.style.display = 'none';
        });

        window.addEventListener('click', function(event) {
            if (event.target == modal) {
                modal.style.display = 'none';
            }
        });

        const consentToggle = document.getElementById('cookieConsentToggle');
        const userConsent = getCookie('user_cookie_consent');

        consentToggle.checked = (userConsent === 'true');
        darkElements = document.getElementsByClassName('dark-mode-toggle-container');

        consentToggle.addEventListener('change', () => {
            const consentValue = consentToggle.checked ? 'true' : 'false';
            setCookie('user_cookie_consent', consentValue, 365);
            for (var i = 0; i < darkElements.length; i++) {
                darkElements[i].style.display = consentToggle.checked ? 'flex' : 'none';
            }
        });
    }

    const darkModeToggle = document.getElementById('darkModeToggle');
    // Event Listener für den Dark Mode Slider
    if (darkModeToggle){
        darkModeToggle.addEventListener('change', () => {
          const darkMode = darkModeToggle.checked;
          if (darkMode) {
            setCookie('dark', 'true', 30);
            setFooterHeader()
          } else {
            setCookie('dark', 'false', 30);
            setFooterHeader()
          }
          document.getElementById('darkModeStylesheet').disabled = !darkMode;
        });
        darkModeToggle.checked = isDarkMode;
    }

});

