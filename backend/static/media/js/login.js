/**
 * @file Logik für den Login und Register
 */

/**
 * Füllt das Dropdown-Menü für Fachrichtungen mit Optionen, die aus dem globalen Array `fachrichtungen` generiert werden.
 * Jede Option repräsentiert eine Fachrichtung, die als Auswahlmöglichkeit zur Verfügung steht.
 */
function fillFachrichtungDropdown() {
	const fachrichtungDropdown = document.getElementById('fachrichtungDropdown');
	fachrichtungen.forEach(fachrichtung => {
		const option = document.createElement('option');
		option.value = fachrichtung;
		option.textContent = fachrichtung;
		fachrichtungDropdown.appendChild(option);
	});
}

/**
 * Wechselt die Sichtbarkeit der Anmelde- und Registrierungsformulare und markiert den entsprechenden Tab als aktiv.
 * `showLogin` macht das Anmeldeformular sichtbar und versteckt das Registrierungsformular.
 */
function showLogin() {
	document.getElementById('loginForm').style.display = 'block';
	document.getElementById('signUpForm').style.display = 'none';
	document.getElementById('loginTab').classList.add('active');
	document.getElementById('signUpTab').classList.remove('active');
}

/**
 * Wechselt die Sichtbarkeit der Anmelde- und Registrierungsformulare und markiert den entsprechenden Tab als aktiv.
 * `showSignUp` macht das Registrierungsformular sichtbar und versteckt das Anmeldeformular.
 */
function showSignUp() {
	document.getElementById('loginForm').style.display = 'none';
	document.getElementById('signUpForm').style.display = 'block';
	document.getElementById('loginTab').classList.remove('active');
	document.getElementById('signUpTab').classList.add('active');
}

 /**
 * Führt den Login-Prozess durch Senden einer POST-Anfrage mit E-Mail und Passwort an den Authentifizierungsendpunkt aus.
 * @param {string} email Die E-Mail-Adresse des Benutzers.
 * @param {string} password Das Passwort des Benutzers.
 * @returns {Promise<boolean>} Gibt ein Promise zurück, das bei erfolgreicher Authentifizierung `true` und sonst `false` löst.
 */
async function login(email, password) {
    const url = 'http://167.172.184.62/api/v1/token/';

    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email, password })
        });

        if (!response.ok) {
            throw new Error(`Anfrage fehlgeschlagen: ${response.status}`);
        }

        const data = await response.json();
        localStorage.setItem('access', data.access);
        return true;
    } catch (error) {
        console.error('Fehler beim Einloggen:', error);
        return false;
    }
}

/**
 * Versucht, einen Benutzer anzumelden, indem sie E-Mail und Passwort an die `login`-Funktion übergibt und auf deren Ergebnis reagiert.
 * Zeigt Fehlermeldungen an, wenn der Login-Versuch fehlschlägt oder die Authentifizierungsdetails ungültig sind.
 * @param {boolean} isLogin Gibt an, ob die Funktion im Kontext eines Login-Versuchs aufgerufen wird.
 * @param {HTMLInputElement} mail Das E-Mail-Eingabefeld, falls `isLogin` `false` ist.
 * @param {HTMLInputElement} pw Das Passwort-Eingabefeld, falls `isLogin` `false` ist.
 */
async function attemptLogin(isLogin, mail, pw) {
    showLoginErrorMessage('Login...', 1);
	let email;
	let password;

	if (isLogin) {
		email = document.getElementById('emailInput').value;
		password = document.getElementById('passwordInput').value;
	} else {
		email = mail.value;
		password = pw.value;	
	}
    try {
        const loginSuccess = await login(email, password);
        if (loginSuccess) {
            const userData = await fetchUserData();
            if (userData && userData.email === email) {
                localStorage.setItem('accountId', userData.id);
                localStorage.setItem('accountName', userData.username);
                localStorage.setItem('accountPhoto', userData.photo);
                setCookieIfNotSet('dark', 'false', 30);
                window.location.href = 'index.html';
            } else {
                showLoginErrorMessage('Falsche E-Mail oder Passwort', 2);
            }
        } else {
            showLoginErrorMessage('Falsche E-Mail oder Passwort', 2);
        }
    } catch (error) {
        console.error('Fehler beim Einloggen:', error);
        showLoginErrorMessage('Fehler beim Einloggen. Bitte versuchen Sie es später erneut.');
    }
}

/**
 * Zeigt eine Fehler- oder Hinweismeldung im Login-Formular an. Bestehende Nachrichten werden vor dem Anzeigen der neuen Nachricht entfernt.
 * @param {string} message Die anzuzeigende Nachricht.
 * @param {number} type Der Typ der Nachricht (1 für Hinweis, 2 für Fehler).
 */
function showLoginErrorMessage(message, type) {
    const loginForm = document.getElementById('loginForm');
    const existingError = loginForm.querySelector('.login-error-message');
    const existingTry = loginForm.querySelector('.login-try-message');
    if (existingError) existingError.remove();
    if (existingTry) existingTry.remove();

    const errorMessage = document.createElement('div');
    errorMessage.textContent = message;
    if (type == 1) {
        errorMessage.className = 'login-try-message ';
    }
    if (type == 2) {
        errorMessage.className = 'login-error-message';
    }
    loginForm.insertBefore(errorMessage, loginForm.children[2]);
}

/**
 * Fügt dem Login-Button einen Event Listener hinzu, der das Standard-Formular-Submit-Verhalten unterbindet und stattdessen
 */
document.querySelector('#loginForm button').addEventListener('click', function(event) {
	event.preventDefault();
	attemptLogin(true, "mail", "pw");
});


/**
 * Ein Event Listener, der auf das vollständige Laden des Dokuments wartet, bevor Funktionen aufgerufen werden, die
 * das Dropdown-Menü für Fachrichtungen füllen und die Anmeldeansicht initial anzeigen.
 */
document.addEventListener('DOMContentLoaded', function() {
	fillFachrichtungDropdown();
	showLogin();
});

/**
 * Überprüft, ob zwei Passworteingaben übereinstimmen.
 *
 * @param {string} password - Das Passwort.
 * @param {string} confirmPassword - Die Passwortbestätigung.
 * @returns {boolean} Gibt `true` zurück, wenn beide Passwörter identisch sind, sonst `false`.
 */
function checkPasswordMatch(password, confirmPassword) {
    return password === confirmPassword;
}

/**
 * Zeigt ein Erfolgssymbol neben einem Eingabefeld an und entfernt jegliche vorhandene Fehlermeldungen, um eine erfolgreiche Validierung zu signalisieren.
 *
 * @param {HTMLElement} inputElement - Das Eingabeelement, das validiert wurde.
 */
function showSuccess(inputElement) {
    const existingSuccessIcon = inputElement.parentNode.querySelector('.success-icon');
    if (!existingSuccessIcon) {
        const icon = document.createElement('i');
        icon.className = 'fa fa-check success-icon';
        inputElement.parentNode.appendChild(icon);
    }

    inputElement.classList.add('valid');
    inputElement.classList.remove('invalid');

    const existingError = inputElement.parentNode.querySelector('.error-message');
    if (existingError) existingError.remove();
}

/**
 * Zeigt eine Fehlermeldung in der Nähe eines Eingabefeldes an und entfernt jegliche vorhandene Erfolgssymbole, um auf einen Validierungsfehler hinzuweisen.
 *
 * @param {HTMLElement} inputElement - Das Eingabeelement, bei dem der Fehler aufgetreten ist.
 * @param {string} message - Die Fehlermeldung, die angezeigt werden soll.
 */
function showErrorMessage(inputElement, message) {
    const existingError = inputElement.parentNode.querySelector('.error-message');
    const existingSuccessIcon = inputElement.parentNode.querySelector('.success-icon');
    if (existingError) existingError.remove();
    if (existingSuccessIcon) existingSuccessIcon.remove();

    const errorMessage = document.createElement('div');
    errorMessage.textContent = message;
    errorMessage.classList.add('error-message');
    inputElement.parentNode.appendChild(errorMessage);

    inputElement.classList.add('invalid');
    inputElement.classList.remove('valid');
}

/**
 * Validiert das Passwort und dessen Bestätigung im Registrierungsformular. Zeigt Fehlermeldungen an, wenn die Validierung fehlschlägt.
 */
function validatePasswordAndConfirm() {
    const passwordInput = document.getElementById('signUpPassword');
    const confirmPasswordInput = document.getElementById('signUpConfirmPassword');
    let isValidPassword = validatePassword(passwordInput.value);
    let isMatch = checkPasswordMatch(passwordInput.value, confirmPasswordInput.value);

    if (!isValidPassword) {
		showErrorMessage(passwordInput, 'Das Passwort muss mindestens 8 Zeichen lang sein, mindestens einen Buchstaben, eine Zahl und ein Sonderzeichen (@$!%*?&#+-._) enthalten.');
    } else {
        showSuccess(passwordInput);
    }

    if (!isMatch) {
        showErrorMessage(confirmPasswordInput, 'Passwörter stimmen nicht überein.');
    } else {
        showSuccess(confirmPasswordInput);
    }
}

document.getElementById('registerButton').addEventListener('click', attemptRegistration);

/**
 * Führt den Registrierungsprozess durch Validierung der Eingabefelder und Senden der Benutzerdaten an den Registrierungsendpunkt aus.
 * Zeigt Fehlermeldungen an, wenn die Registrierung fehlschlägt oder die Eingabedaten ungültig sind.
 */
async function attemptRegistration() {
	const emailInput = document.getElementById('signUpEmail');
	const passwordInput = document.getElementById('signUpPassword');
	const confirmPasswordInput = document.getElementById('signUpConfirmPassword');
	const usernameInput = document.getElementById('usernameInput');
	const fachrichtungDropdown = document.getElementById('fachrichtungDropdown');
	const registerButton = document.getElementById('registerButton');
	registerButton.disabled = true;
	const allFieldsFilled = emailInput.value.trim() && 
							passwordInput.value.trim() &&
							confirmPasswordInput.value.trim() &&
							usernameInput.value.trim() &&
							fachrichtungDropdown.value;

	const allFieldsValid = allFieldsFilled &&
						   emailInput.classList.contains('valid') &&
						   passwordInput.classList.contains('valid') &&
						   confirmPasswordInput.classList.contains('valid') &&
						   usernameInput.classList.contains('valid');

	if (!allFieldsFilled || !allFieldsValid) {
		showRegistrationErrorMessage('Bitte füllen Sie alle Felder korrekt aus.');
		return;
	}

	if (!await registerUser(emailInput.value, passwordInput.value, usernameInput.value)) {
		showRegistrationErrorMessage('Registrierung fehlgeschlagen.');
		registerButton.disabled = false; 
		return;
	} else {
		attemptLogin(false, emailInput, passwordInput);
	}
}

let isRegistrationInProgress = false;

/**
 * Sendet eine Registrierungsanfrage an den Server mit den Benutzerdaten (E-Mail, Passwort, Benutzername).
 * @param {string} email Die E-Mail-Adresse des Benutzers.
 * @param {string} password Das Passwort des Benutzers.
 * @param {string} username Der Benutzername des Benutzers.
 * @returns {Promise<boolean>} Gibt ein Promise zurück, das bei erfolgreicher Registrierung `true` löst, sonst `false`.
 */
async function registerUser(email, password, username) {
    if (isRegistrationInProgress) return;
    isRegistrationInProgress = true;

    const url = 'http://167.172.184.62/api/v1/user/';
    const userData = {
        email: email,
        password: password,
        username: username
    };

    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        });

        const responseData = await response.json(); 

        if (!response.ok) {
            if (responseData.email && responseData.email.includes("custom user mit diesem email existiert bereits.")) {
                const emailInput = document.getElementById('signUpEmail');
                showErrorMessage(emailInput, "Ein Benutzer mit dieser E-Mail existiert bereits.");
            } else {
                showLoginErrorMessage('Fehler aufgetreten');
            }
            throw new Error(`Fehler bei der Registrierung: ${response.status}`);
        }
        
        return true;
    } catch (error) {
        console.error('Registrierungsfehler:', error);
        return false;
    } finally {
        isRegistrationInProgress = false;
    }
}

/**
 * Zeigt eine Fehlermeldung im Registrierungsformular an. Bestehende Fehlermeldungen werden vor dem Hinzufügen einer neuen entfernt.
 *
 * @param {string} message - Die anzuzeigende Fehlermeldung.
 */
function showRegistrationErrorMessage(message) {
	const signUpForm = document.getElementById('signUpForm');
	const existingError = signUpForm.querySelector('.registration-error-message');
	if (existingError) existingError.remove();

	const errorMessage = document.createElement('div');
	errorMessage.textContent = message;
	errorMessage.classList.add('registration-error-message');
	signUpForm.appendChild(errorMessage);
}

/**
 * Fügt dem LoginForm einen Event Listener hinzu, der auf das Keypress-Ereignis reagiert.
 * Wenn die Enter-Taste (keyCode 13) gedrückt wird, wird das Standardverhalten des Events verhindert und der Login-Versuch gestartet.
 */
document.getElementById('loginForm').addEventListener('keypress', function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        attemptLogin(true);
    }
});

/**
 * Fügt dem SignUpForm einen Event Listener hinzu, der auf das Keypress-Ereignis reagiert.
 * Wenn die Enter-Taste (keyCode 13) gedrückt wird, wird das Standardverhalten des Events verhindert und der Registrierungsversuch gestartet.
 */
document.getElementById('signUpForm').addEventListener('keypress', function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        attemptRegistration();
    }
});

/**
 * Fügt dem SignUpForm einen Event Listener für das Input-Ereignis hinzu. Dieser Listener validiert Benutzereingaben in Echtzeit.
 * Abhängig von der ID des Eingabeelements werden unterschiedliche Validierungslogiken angewandt:
 * - `signUpEmail`: Überprüft, ob die E-Mail-Adresse gültig ist und den Anforderungen entspricht.
 * - `signUpPassword` und `signUpConfirmPassword`: Führt eine Passwortvalidierung durch, inklusive der Überprüfung auf Übereinstimmung.
 * - `usernameInput`: Überprüft, ob der Benutzername den Vorgaben entspricht.
 *
 * Bei gültigen Eingaben wird eine Erfolgsmarkierung angezeigt, bei ungültigen Eingaben wird eine Fehlermeldung angezeigt.
 */
document.getElementById('signUpForm').addEventListener('input', function(event) {
    const inputElement = event.target;
    const inputId = inputElement.id;

    let isValid = true;

	
    if (inputId === 'signUpEmail') {
        isValid = validateEmail(inputElement.value);
        if (!isValid) { 
            showErrorMessage(inputElement, 'Ungültige E-Mail-Adresse. Es sind nur IU Mail-Adressen erlaubt.');
        }
    } else if (inputId === 'signUpPassword' || inputId === 'signUpConfirmPassword') {
        validatePasswordAndConfirm();
        return; 
    } else if (inputId === 'usernameInput') {
        isValid = validateUsername(inputElement.value);
        if (!isValid) {
			showErrorMessage(inputElement, 'Der Benutzername muss zwischen 5 und 25 Zeichen lang sein. Es sind nur Buchstaben, Zahlen und Leerzeichen erlaubt.');
		}
    } 

    if (isValid) {
        showSuccess(inputElement);
    }
	

});

