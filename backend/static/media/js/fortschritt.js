/**
 * @file Logik für die Statistik
 */


/**
 * Aktualisiert den Lernfortschritt eines Nutzers basierend auf seinen Statistiken.
 * Diese asynchrone Funktion holt die Kurs-ID aus der URL, die Benutzer-ID aus dem lokalen Speicher,
 * lädt alle Fragen und Benutzerstatistiken und aktualisiert die Benutzeroberfläche mit den berechneten Werten.
 */
async function updateFortschritt() {
    // Holt die Kurs-ID aus der URL und die Benutzer-ID aus dem lokalen Speicher.
    const kursId = getQueryVariable('kurs_id');
    const userId = localStorage.getItem('accountId');
    
    // Lädt alle Fragen und Benutzerstatistiken asynchron.
    fragen = await fetchAllQuestions();
    const stats = await fetchStats();
    const userStats = stats.find(stat => stat.kurs == kursId && stat.user == userId);

    // Überprüft, ob Statistiken für den aktuellen Benutzer vorhanden sind.
    if (userStats) {
        // Berechnet den Gesamtfortschritt und die Genauigkeit der Antworten.
        const totalFragen = userStats.lernmodus_richtig + userStats.quizshow_richtig + userStats.liveevent_richtig;
        const totalFalsch = userStats.lernmodus_falsch + userStats.quizshow_falsch + userStats.liveevent_falsch; 
        const prozentRichtig = totalFragen / (totalFragen + totalFalsch) * 100;
        
        // Animiert die Anzeige der Statistiken.
        animateValue(document.querySelector('.grid-container .grid-item:nth-child(2) .number'), 0, userStats.quizshow_richtig, 1000);
        animateValue(document.querySelector('.grid-container .grid-item:nth-child(3) .number'), 0, prozentRichtig, 1000, true); 
        animateValue(document.querySelector('.grid-container .grid-item:nth-child(4) .number'), 0, userStats.liveevent_richtig, 1000);

        // Passt die Farbe des Prozentelements basierend auf dem Prozentsatz der richtigen Antworten an.
        const prozentElement = document.querySelector('.grid-container .grid-item:nth-child(3) .number');
        prozentElement.style.color = getColorForPercentage(prozentRichtig);
    } else {
        // Setzt Standardwerte, falls keine Statistiken gefunden wurden.
        document.querySelectorAll('.grid-container .grid-item .number').forEach(item => {
            item.textContent = '0';
            item.style.color = 'black'; 
        });
    }
    
    // Aktualisiert die Anzahl der vom Benutzer erstellten Fragen, falls vorhanden.
    if (fragen) {
        const erstellteFragenCount = fragen.filter(frage => frage.author == userId && frage.kurs == kursId).length;
        animateValue(document.querySelector('.grid-container .grid-item:nth-child(1) .number'), 0, erstellteFragenCount, 1000);
    }
}


/**
 * Ermittelt eine Farbe basierend auf dem Prozentsatz der richtigen Antworten.
 * @param {number} percentage - Der Prozentsatz der richtigen Antworten.
 * @returns {string} Die CSS-Farbe, die dem Prozentsatz entspricht.
 */
function getColorForPercentage(percentage) {
	if (percentage < 20) {
		return 'red';
	} else if (percentage < 40) {
		return 'darkorange';
	} else if (percentage < 60) {
		return '#FFD700';
	} else if (percentage < 80) {
		return 'lightgreen';
	} else {
		return 'darkgreen';
	}
}


/**
 * Animiert den Wert eines Elementes von einem Startwert zu einem Endwert über eine bestimmte Dauer.
 * @param {Element} element - Das DOM-Element, dessen Inhalt animiert wird.
 * @param {number} start - Der Anfangswert der Animation.
 * @param {number} end - Der Endwert der Animation.
 * @param {number} duration - Die Dauer der Animation in Millisekunden.
 * @param {boolean} isPercentage - Gibt an, ob der Wert als Prozentsatz formatiert werden soll.
 */
function animateValue(element, start, end, duration, isPercentage = false) {
	let startTimestamp = null;
	const step = (timestamp) => {
	if (!startTimestamp) startTimestamp = timestamp;
		const progress = Math.min((timestamp - startTimestamp) / duration, 1);
		element.textContent = Math.floor(progress * (end - start) + start) + (isPercentage ? '%' : '');
			if (progress < 1) {
			  window.requestAnimationFrame(step);
			}
	};
	window.requestAnimationFrame(step);
}


/**
 * Initialisiert die Aktualisierung des Fortschritts und den Titel der Box, wenn das DOM vollständig geladen ist.
 */
document.addEventListener('DOMContentLoaded', function() {
	updateFortschritt();
	updateTitleBox();
});