from django.conf import settings
from django.db import models
from kurse.models import Kurs  # Import the Kurs model if not already imported
from django.contrib.auth.models import User  # Import the User model if not already imported

class Frage(models.Model):
    SCHWIERIGKEIT_CHOICES = (
        (1, 'Einfach'),
        (2, 'Mittel'),
        (3, 'Schwer'),
    )

    fragetext = models.CharField(max_length=500)
    schwierigkeit = models.IntegerField(choices=SCHWIERIGKEIT_CHOICES)
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    antwort_richtig = models.CharField(max_length=500)
    antwort_falsch_1 = models.CharField(max_length=500)
    antwort_falsch_2 = models.CharField(max_length=500)
    antwort_falsch_3 = models.CharField(max_length=500)
    info_richtig = models.CharField(max_length=500, blank=True)
    info_falsch_1 = models.CharField(max_length=500, blank=True)
    info_falsch_2 = models.CharField(max_length=500, blank=True)
    info_falsch_3 = models.CharField(max_length=500, blank=True)
    kurs = models.ForeignKey(Kurs, on_delete=models.CASCADE)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f"ID: {self.id} - {self.fragetext}"

    class Meta:
        verbose_name_plural = 'Fragen'