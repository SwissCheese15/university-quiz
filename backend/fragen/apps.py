from django.apps import AppConfig


class FragenConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fragen'
