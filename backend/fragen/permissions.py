from rest_framework import permissions


class CanUpdateQuestion(permissions.BasePermission):
    """
    Custom permission to allow updating a question if it has 30% or more dislikes.
    """

    def has_object_permission(self, request, view, obj):
        # Allow read permissions for any authenticated user
        if request.method in permissions.SAFE_METHODS:
            return True

        # Calculate the total number of likes and dislikes
        total_votes = obj.likes + obj.dislikes

        # Check if the question has 30% or more dislikes
        if total_votes > 0 and (obj.dislikes / total_votes) >= 0.3:
            return True

        # If not, check if the user is the author or an admin
        return obj.author == request.user or request.user.is_superuser
