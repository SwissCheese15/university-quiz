from rest_framework import serializers
from .models import Frage

class FrageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Frage
        fields = '__all__'