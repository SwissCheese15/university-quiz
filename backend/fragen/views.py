from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from drf_spectacular.utils import extend_schema
from .models import Frage
from .serializers import FrageSerializer
from .permissions import CanUpdateQuestion


@extend_schema(tags=["Fragen"])
class FrageViewSet(viewsets.ModelViewSet):
    queryset = Frage.objects.all()
    serializer_class = FrageSerializer
    permission_classes = [IsAuthenticated, CanUpdateQuestion]

    @action(detail=True, methods=['post'], permission_classes=[IsAuthenticated])
    def like(self, request, pk=None):
        question = self.get_object()
        question.likes += 1
        question.save()
        return Response({'likes': question.likes}, status=status.HTTP_200_OK)

    @action(detail=True, methods=['post'], permission_classes=[IsAuthenticated])
    def dislike(self, request, pk=None):
        question = self.get_object()
        question.dislikes += 1
        question.save()
        return Response({'dislikes': question.dislikes}, status=status.HTTP_200_OK)
