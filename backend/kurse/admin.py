from django.contrib import admin
from .models import Kurs

class KursAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'image_url')
    search_fields = ('name', 'code')

admin.site.register(Kurs, KursAdmin)