from django.db import models


class Kurs(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=50)
    image_url = models.CharField(max_length=255)

    def __str__(self):
        return f"ID: {self.id} - {self.name}"

    class Meta:
        verbose_name_plural = 'Kurse'
