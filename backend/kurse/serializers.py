from rest_framework import serializers
from .models import Kurs

class KursSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kurs
        fields = ['id', 'name', 'code', 'image_url']