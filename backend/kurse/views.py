from rest_framework import viewsets
from drf_spectacular.utils import extend_schema
from .permissions import IsAdminOrReadOnly

from .models import Kurs
from .serializers import KursSerializer


@extend_schema(tags=["Kurse"])
class KursViewSet(viewsets.ModelViewSet):
    queryset = Kurs.objects.all()
    serializer_class = KursSerializer
    permission_classes = [IsAdminOrReadOnly]