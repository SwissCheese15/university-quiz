from django.urls import re_path, path
# Third party packages
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

from kurse.views import KursViewSet
from fragen.views import FrageViewSet
from nutzer_kurs_statistik.views import NutzerKursStatistikViewSet, FrageBeantwortenViewSet
from custom_auth.views import CustomUserViewSet, CurrentUserViewSet

app_name = 'v1'

# Setup your api urls here with router.register(...)
router = DefaultRouter()

router.register(r'kurs', KursViewSet)
router.register(r'frage', FrageViewSet)
router.register(r'frage-nutzer-statistik', NutzerKursStatistikViewSet)
router.register(r'user', CustomUserViewSet, basename='user')

urlpatterns = router.urls + [
    re_path(r'token/$', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    re_path(r'auth/token/verify/$', TokenVerifyView.as_view(), name='token_verify'),
    re_path(r'token/refresh/$', TokenRefreshView.as_view(), name='token_refresh'),
    # Swagger related urls
    re_path(r"schema/", SpectacularAPIView.as_view(), name="schema"),
    re_path(r"docs/$", SpectacularSwaggerView.as_view(url_name="schema"), name="swagger-ui"),
    re_path(r"redoc/$", SpectacularRedocView.as_view(url_name="schema"), name="redoc"),
    re_path(r'^current_user/$', CurrentUserViewSet.as_view({
        'get': 'retrieve',
        'put': 'update',
        'delete': 'destroy'
    }), name='current-user'),
    path('beantworten/<int:kurs_id>/increment-lernmodus-richtig/', FrageBeantwortenViewSet.as_view({'post': 'increment_lernmodus_richtig'}), name='increment-lernmodus-richtig'),
    path('beantworten/<int:kurs_id>/increment-lernmodus-falsch/', FrageBeantwortenViewSet.as_view({'post': 'increment_lernmodus_falsch'}), name='increment-lernmodus-falsch'),
    path('beantworten/<int:kurs_id>/increment-quizshow-richtig/', FrageBeantwortenViewSet.as_view({'post': 'increment_quizshow_richtig'}), name='increment-quizshow-richtig'),
    path('beantworten/<int:kurs_id>/increment-quizshow-falsch/', FrageBeantwortenViewSet.as_view({'post': 'increment_quizshow_falsch'}), name='increment-quizshow-falsch'),
    path('beantworten/<int:kurs_id>/increment-liveevent-richtig/', FrageBeantwortenViewSet.as_view({'post': 'increment_liveevent_richtig'}), name='increment-liveevent-richtig'),
    path('beantworten/<int:kurs_id>/increment-liveevent-falsch/', FrageBeantwortenViewSet.as_view({'post': 'increment_liveevent_falsch'}), name='increment-liveevent-falsch'),
]