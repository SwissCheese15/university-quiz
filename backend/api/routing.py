from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from django.urls import path
from multiplayer.consumers import (
    QuizConsumer1,
    QuizConsumer2,
    QuizConsumer4,
    QuizConsumer5,
    QuizConsumer6,
    QuizConsumer7,
    QuizConsumer8,
    QuizConsumer9,
)

websocket_application = ProtocolTypeRouter({
    "websocket": AuthMiddlewareStack(
        URLRouter([
            path("ws-quiz-show/1/", QuizConsumer1.as_asgi()),
            path("ws-quiz-show/2/", QuizConsumer2.as_asgi()),
            path("ws-quiz-show/4/", QuizConsumer4.as_asgi()),
            path("ws-quiz-show/5/", QuizConsumer5.as_asgi()),
            path("ws-quiz-show/6/", QuizConsumer6.as_asgi()),
            path("ws-quiz-show/7/", QuizConsumer7.as_asgi()),
            path("ws-quiz-show/8/", QuizConsumer8.as_asgi()),
            path("ws-quiz-show/9/", QuizConsumer9.as_asgi()),
        ])
    ),
})