from django.urls import include, path, re_path

from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView
)
from rest_framework.versioning import URLPathVersioning

from api.urls_v1 import urlpatterns as urls_v1

default_version = 'v1'

urlpatterns = [
    re_path(
        r'(?P<version>(v1))/schema/',
        SpectacularAPIView.as_view(versioning_class=URLPathVersioning),
        name='schema'
    ),
    re_path(r'(?P<version>(v1))/docs/$', SpectacularSwaggerView.as_view(url='../schema/'), name='swagger-ui'),
    re_path(r'(?P<version>(v1))/redoc/$', SpectacularRedocView.as_view(url='../schema/'), name='redoc'),

    re_path(
        r'docs/$',
        SpectacularSwaggerView.as_view(url=f'../{default_version}/schema/'),
        name='swagger-ui'
    ),
    re_path(
        r'redoc/$',
        SpectacularRedocView.as_view(url=f'../{default_version}/schema/'),
        name='redoc'
    ),

    path('v1/', include((urls_v1, 'v1'), namespace='v1')),
]
