/**
 * @file Logik für die Quizshow (EINZELSPIELER)
 */

document.addEventListener('DOMContentLoaded', async function() {
	const kursId = getQueryVariable("kurs_id");
	let currentQuestionIndex = 0;
	let currentPoints = 0;
	let selectedQuestions = await selectRandomQuestions(kursId);
	let safetyNetActive = false;
	
	
	await loadQuestion(selectedQuestions[currentQuestionIndex]);

    /**
     * Wählt eine zufällige Auswahl von Fragen für das Quiz basierend auf der Kurs-ID.
     * Die Fragen werden nach Schwierigkeitsgrad gefiltert und ausgewählt, um eine ausgewogene Mischung zu gewährleisten.
     *
     * @param {string} kursId - Die ID des Kurses, aus dem die Fragen ausgewählt werden sollen.
     * @returns {Promise<Array>} Eine Liste ausgewählter Fragen für das Quiz.
     */
	async function selectRandomQuestions(kursId) {
		fragen = await fetchAllQuestions()
		const filteredQuestions = fragen.filter(question => question.kurs === parseInt(kursId));

		const difficulty1 = filteredQuestions.filter(question => question.schwierigkeit === 1);
		const difficulty2 = filteredQuestions.filter(question => question.schwierigkeit === 2);
		const difficulty3 = filteredQuestions.filter(question => question.schwierigkeit === 3);

		const selectedDifficulty1 = pickRandomQuestions(difficulty1, 3);
		const selectedDifficulty2 = pickRandomQuestions(difficulty2, 4);
		const selectedDifficulty3 = pickRandomQuestions(difficulty3, 3);

		return [...selectedDifficulty1, ...selectedDifficulty2, ...selectedDifficulty3];
	}

    /**
     * Lädt und zeigt die aktuelle Quizfrage und die dazugehörigen Antwortmöglichkeiten an.
     * Mischt die Antwortmöglichkeiten und setzt Event-Listener für die Auswahl einer Antwort.
     *
     * @param {Object} question - Das Frageobjekt, das angezeigt werden soll.
     */
	function loadQuestion(question) {
		document.getElementById('counter').style.display = 'none';
		document.getElementById('question-text').innerHTML = `<p>${question.fragetext}</p>`;
		let answers = [question.antwort_richtig, question.antwort_falsch_1, question.antwort_falsch_2, question.antwort_falsch_3];
		document.getElementById('info_quizshow').style.display = 'none';
		console.log(answers)
		shuffleArray(answers);
		const answerList = document.getElementById('answer-list');
		answerList.innerHTML = '';

        const thumbsUpButtons = document.querySelectorAll(".thumbs-up .fa-thumbs-up");
        const thumbsDownButtons = document.querySelectorAll(".thumbs-up .fa-thumbs-down");

        const questionId = question.id;

        thumbsUpButtons.forEach(button => {
            button.setAttribute("data-question-id", questionId);
        });

        thumbsDownButtons.forEach(button => {
            button.setAttribute("data-question-id", questionId);
        });


		answers.forEach(answer => {
            let li = document.createElement('li');
            li.textContent = answer;
            li.onclick = () => handleAnswerSelection(answer, li);
            answerList.appendChild(li);
		});
	}

    /**
     * Wählt eine zufällige Teilmenge von Fragen aus einer gegebenen Liste aus.
     *
     * @param {Array} questions - Die Liste der Fragen, aus denen ausgewählt wird.
     * @param {number} count - Die Anzahl der auszuwählenden Fragen.
     * @returns {Array} Eine zufällig ausgewählte Teilmenge der Fragen.
     */
	function pickRandomQuestions(questions, count) {
		shuffleArray(questions);
		return questions.slice(0, count);
	}

    /**
     * Mischt ein Array in Place mithilfe des Durstenfeld-Shuffle-Algorithmus.
     *
     * @param {Array} array - Das Array, das gemischt werden soll.
     */
	function shuffleArray(array) {
		for (let i = array.length - 1; i > 0; i--) {
			let j = Math.floor(Math.random() * (i + 1));
			[array[i], array[j]] = [array[j], array[i]];
		}
	}

    /**
     * Verarbeitet die Auswahl einer Antwort, zeigt Feedback an und lädt die nächste Frage nach einer kurzen Verzögerung.
     * Aktualisiert außerdem die Punktzahl und Statistiken basierend auf der Richtigkeit der Antwort.
     *
     * @param {string} selectedAnswer - Die ausgewählte Antwort.
     * @param {HTMLElement} cElement - Das DOM-Element der ausgewählten Antwort.
     */
	function handleAnswerSelection(selectedAnswer, cElement) {
		let question = selectedQuestions[currentQuestionIndex];
		if (selectedAnswer === question.antwort_richtig) {
			document.getElementById('info_quizshow').style.display = 'block';
			document.getElementById('info_quizshow').style.backgroundColor = '#4CAF50';
			document.getElementById('info_quizshow').innerHTML = `<p>${question.info_richtig}</p>`;
			currentPoints++;
			disableClickInteractions();
			safetyNetActive = false;
            cElement.classList.add('correct-answer');
            statAnswer(kursId, "increment-quizshow-richtig")
			let counter = 3;
			const countdownInterval = setInterval(() => {
				if (counter > 0) {
					document.getElementById('counter').style.display = 'block';
					document.getElementById('counter').innerHTML = `${counter--}`;
				} else {
					clearInterval(countdownInterval);
					loadNextQuestion();
					enableClickInteractions();
					updatePointsDisplay();
					enableThumbs()
				}
			}, 1000);

		} else {
			if (safetyNetActive) {
                document.getElementById('info_quizshow').style.display = 'block';
                document.getElementById('info_quizshow').style.backgroundColor = '#4e8ead';
                document.getElementById('info_quizshow').innerHTML = `<p>[SICHERHEITSNETZ] Falsche Antwort. Versuchen Sie es noch einmal.</p>`;
                safetyNetActive = false;

                const answerListItems = document.querySelectorAll('#answer-list li');
                answerListItems.forEach(item => {
                    item.style.pointerEvents = 'all';
                    item.classList.remove('disabled');
                    item.onclick = () => handleAnswerSelection(item.textContent, item);
            });
		} else {
			document.getElementById('info_quizshow').style.display = 'block';
			document.getElementById('info_quizshow').style.backgroundColor = 'red';
			document.getElementById('info_quizshow').innerHTML = `
				<p>${getWrongInfo(selectedAnswer, question)}</p>
				<button id="tryAgainButton">Try Again</button>
				<button id="playLaterButton">Später Weiterspielen</button>
			`;
			document.getElementById('quiz_quizshow').style.display = 'none';

			document.getElementById('tryAgainButton').addEventListener('click', reloadQuiz);
			document.getElementById('playLaterButton').addEventListener('click', backToPreviousPage);

			statAnswer(kursId, "increment-quizshow-falsch")
		}
		}
	}

    /**
     * Gibt die Erklärung für eine falsche Antwort zurück, basierend auf der ausgewählten falschen Antwort.
     *
     * @param {string} selectedAnswer - Die ausgewählte Antwort des Benutzers.
     * @param {Object} question - Das aktuelle Frageobjekt.
     * @returns {string} Die Erklärung, warum die ausgewählte Antwort falsch ist.
     */
	function getWrongInfo(selectedAnswer, question) {
		if (selectedAnswer === question.antwort_falsch_1) return question.info_falsch_1;
		if (selectedAnswer === question.antwort_falsch_2) return question.info_falsch_2;
		return question.info_falsch_3;
	}

    /**
     * Lädt die nächste Frage des Quiz, falls verfügbar. Zeigt das Quizende an, wenn alle Fragen beantwortet wurden.
     */
	function loadNextQuestion() {
		if (currentQuestionIndex < selectedQuestions.length - 1) {
			currentQuestionIndex++;
			loadQuestion(selectedQuestions[currentQuestionIndex]);
			document.getElementById('question-area').style.display = 'block';
		} else {
			endQuiz();
		}
	}

    /**
     * Beendet das Quiz, zeigt eine Abschlussnachricht an und bietet Optionen zum Neustart oder zur Rückkehr zur vorherigen Seite.
     */
    function endQuiz() {
        const quizShow = document.getElementById('quiz_quizshow');
        const infoQuizShow = document.getElementById('info_quizshow');

        quizShow.style.display = 'none';

        infoQuizShow.style.display = 'block';

        infoQuizShow.innerHTML = `
            <i class="fas fa-trophy icon"></i>
            <p>Herzlichen Glückwunsch! Du hast das Quiz abgeschlossen.</p>
            <p><strong>10 Punkte</strong> wurden deinem Konto gutgeschrieben!</p>
            <button id="tryAgainButton">Try Again</button>
            <button id="playLaterButton">Später Weiterspielen</button>
        `;

        document.getElementById('tryAgainButton').addEventListener('click', reloadQuiz);
        document.getElementById('playLaterButton').addEventListener('click', backToPreviousPage);

        generateConfetti();
    }

    /**
     * Generiert eine Konfettianimation, um den Abschluss des Quiz zu feiern.
     */
    function generateConfetti() {
      let confettiWrapper = document.getElementById('confetti-wrapper');
      if (!confettiWrapper) {
        confettiWrapper = document.createElement('div');
        confettiWrapper.id = 'confetti-wrapper';
        document.body.appendChild(confettiWrapper);
      }
      confettiWrapper.innerHTML = '';

      for (let i = 0; i < 100; i++) {
        let confetti = document.createElement('div');
        confetti.className = 'confetti';
        confetti.style.left = `${Math.random() * 100}%`;
        confetti.style.backgroundColor = getRandomColor();

        const size = Math.random() * (15 - 5) + 5;
        confetti.style.width = `${size}px`;
        confetti.style.height = `${size}px`;

        const duration = Math.random() * (5 - 2) + 2;
        confetti.style.animationDuration = `${duration}s`;
        confetti.style.animationDelay = `${Math.random() * 2}s`;
        confettiWrapper.appendChild(confetti);
      }
    }

    /**
     * Wählt eine zufällige Farbe aus einer vorgegebenen Liste aus.
     *
     * @returns {string} Ein Hexadezimal-String, der eine Farbe repräsentiert.
     */
    function getRandomColor() {
      const colors = ['#FF0', '#F00', '#0F0', '#00F', '#F0F', '#0FF'];
      return colors[Math.floor(Math.random() * colors.length)];
    }

    /**
     * Aktualisiert die Anzeige der Punkte im Quiz. Markiert Punkte, die der Spieler bereits erreicht hat,
     * und hebt den nächsten erreichbaren Punkt hervor.
     */
	function updatePointsDisplay() {
		const pointsItems = document.querySelectorAll('.points-item');
		pointsItems.forEach(item => {
			item.classList.remove('selected');
			item.style.backgroundColor = '#F3F3F7';
		});

		for (let i = 0; i < currentPoints; i++) {
			pointsItems[9 - i].style.backgroundColor = '#4CAF50';
		}

		if (currentPoints < 10) {
			pointsItems[9 - currentPoints].classList.add('selected');
			pointsItems[9 - currentPoints].style.backgroundColor = ''; 
		}
	}

    /**
     * Extrahiert die falschen Antworten aus einem Frageobjekt.
     *
     * @param {Object} question - Das Frageobjekt, aus dem die falschen Antworten extrahiert werden.
     * @returns {Array<string>} Eine Liste der falschen Antworten.
     */
	function getWrongAnswers(question) {
		return [question.antwort_falsch_1, question.antwort_falsch_2, question.antwort_falsch_3];
	}

    /**
     * Entfernt eine angegebene Anzahl von falschen Antworten zufällig aus der Anzeigeliste.
     *
     * @param {Array<string>} wrongAnswers - Die Liste der falschen Antworten.
     * @param {number} count - Die Anzahl der zu entfernenden falschen Antworten.
     */
	function removeRandomWrongAnswers(wrongAnswers, count) {
		shuffleArray(wrongAnswers);
		let removedCount = 0;
		const answerElements = document.querySelectorAll("#answer-list li");

		answerElements.forEach(answerElement => {
			if (removedCount < count && wrongAnswers.includes(answerElement.textContent)) {
				answerElement.remove();
				removedCount++;
			}
		});
	}

    /**
     * Wählt eine zufällige Frage mit demselben Schwierigkeitsgrad aus, die noch nicht gestellt wurde.
     *
     * @param {number} difficulty - Der Schwierigkeitsgrad der Fragen.
     * @returns {Promise<number>} Der Index der neuen Frage im Array der ausgewählten Fragen.
     */
	async function getRandomQuestionIndexWithSameDifficulty(difficulty) {
		fragen = await fetchAllQuestions()
		const availableQuestions = fragen.filter(q => q.schwierigkeit === difficulty && q.kurs === parseInt(kursId) && !selectedQuestions.map(sq => sq.id).includes(q.id));
		if (availableQuestions.length > 0) {
			let randomIndex = Math.floor(Math.random() * availableQuestions.length);
			let newQuestion = availableQuestions[randomIndex];

			selectedQuestions[currentQuestionIndex] = newQuestion;
			return selectedQuestions.indexOf(newQuestion);
		} else {
			return currentQuestionIndex;
		}
	}

    /**
     * Zeigt einen Hinweis für die aktuelle Frage an. Wählt zufällig eine der falschen Antworterklärungen als Hinweis.
     *
     * @param {Object} question - Das Frageobjekt, für das der Hinweis angezeigt wird.
     */
	function displayHint(question) {
		let wrongInfos =  [question.info_falsch_1, question.info_falsch_2, question.info_falsch_3];
		document.getElementById('info_quizshow').style.backgroundColor = '#4e8ead';
		document.getElementById('info_quizshow').style.display = 'block';
		document.getElementById('info_quizshow').textContent = "[TIPP] " + wrongInfos[Math.floor(Math.random() * wrongInfos.length)];
	}

    /**
     * `reloadQuiz` lädt das Quiz neu, um es von vorne zu beginnen.
     */
	function reloadQuiz() {
		window.location.reload();
	}

    /**
     * `backToPreviousPage` navigiert zurück zur vorherigen Seite im Browserverlauf.
     */
	function backToPreviousPage() {
		window.history.back();
	}

    /**
     * Steuern die Klickinteraktionen auf der Seite. `disableClickInteractions` deaktiviert Klicks auf alle interaktiven Elemente,
     */
	function disableClickInteractions() {
		document.querySelectorAll('button, a, #answer-list li').forEach(element => {
			element.style.pointerEvents = 'none';
		});
	}

    /**
     * Steuern die Klickinteraktionen auf der Seite. `disableClickInteractions` aktiviert Klicks auf alle interaktiven Elemente,
     */
	function enableClickInteractions() {
		document.querySelectorAll('button, a, #answer-list li').forEach(element => {
			element.style.pointerEvents = 'all';
		});
	}
	
    /**
     * Fügt dem 50/50-Joker-Button einen Event Listener hinzu.
     * Bei Klick werden zufällig zwei falsche Antworten von der aktuellen Frage entfernt,
     * und der Joker wird deaktiviert, um eine erneute Verwendung zu verhindern.
     */
	document.querySelector('.fifty-fifty').addEventListener('click', function() {
		let wrongAnswers = getWrongAnswers(selectedQuestions[currentQuestionIndex]);
		removeRandomWrongAnswers(wrongAnswers, 2);
		this.classList.add('disabled');
		this.removeEventListener('click', arguments.callee);
	});

    /**
     * Fügt dem Frage-tauschen-Joker-Button einen Event Listener hinzu.
     * Bei Klick wird die aktuelle Frage durch eine andere Frage mit demselben Schwierigkeitsgrad ersetzt,
     * und der Joker wird deaktiviert. Diese Funktion ist asynchron, da sie auf das Laden der neuen Frage wartet.
     */
	document.querySelector('.question-swap').addEventListener('click', async function() {
		if (selectedQuestions.length > 1) {
			let newIndex = await getRandomQuestionIndexWithSameDifficulty(selectedQuestions[currentQuestionIndex].schwierigkeit);


			currentQuestionIndex = newIndex;
			loadQuestion(selectedQuestions[currentQuestionIndex]);

		}
		this.classList.add('disabled');
		this.removeEventListener('click', arguments.callee);
	});

    /**
     * Fügt dem Hinweis-Joker-Button einen Event Listener hinzu.
     * Bei Klick wird ein Hinweis zur aktuellen Frage angezeigt, indem eine zufällige falsche Antwortinformation verwendet wird.
     * Der Joker wird nach Gebrauch deaktiviert, um eine erneute Verwendung zu verhindern.
     */
	document.querySelector('.hint').addEventListener('click', function() {
		displayHint(selectedQuestions[currentQuestionIndex]);
		displayHint(selectedQuestions[currentQuestionIndex]);
		this.classList.add('disabled');
		this.removeEventListener('click', arguments.callee);
	});

    /**
     * Fügt dem Sicherheitsnetz-Joker-Button einen Event Listener hinzu.
     * Bei Klick wird das Sicherheitsnetz aktiviert, was dem Benutzer eine zusätzliche Chance gibt,
     * falls die nächste Antwort falsch ist. Der Joker wird nach Gebrauch deaktiviert.
     */
	document.querySelector('.safety-net').addEventListener('click', function() {
		safetyNetActive = true;
		this.classList.add('disabled');
		this.removeEventListener('click', arguments.callee);
	});
});
