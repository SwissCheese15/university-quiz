/**
 * @file Standard Funktion zum mehrmals verwenden
 */

/**
 * Ein langer String mit Lorem Ipsum-Text, oft verwendet als Platzhalter in Dokumenten oder Webseiten, um zu demonstrieren, wie Text in einem Layout aussehen würde.
 */
var lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

/**
 * Ein Array von Objekten, die häufig gestellte Fragen (FAQs) repräsentieren.
 * Jedes Objekt enthält vier Schlüssel-Wert-Paare:
 * - `id`: Eindeutige Identifikationsnummer der Frage.
 * - `kategorie`: Die Kategorie, zu der die Frage gehört. Ermöglicht die Gruppierung ähnlicher Fragen.
 * - `frage`: Der Text der gestellten Frage.
 * - `antwort`: Die Antwort auf die gestellte Frage.
 */
const faqs = [
	{ id: 1, kategorie: "Profil & Login", frage: "Wie kann ich mein Passwort ändern?", antwort: "Du kannst dein Passwort in deinem Profil (oben rechts) durch die Eingabe eines neuen Passworts im Passwortfeld ändern." },
	{ id: 2, kategorie: "Profil & Login", frage: "Wie kann ich meinen Benutzernamen ändern?", antwort: "Du kannst deinen Benutzernamen in deinem Profil (oben rechts) durch die Eingabe deines neuen Benutzernamen im Benutzernamenfeld ändern." },
	{ id: 3, kategorie: "Profil & Login", frage: "Wie kann ich meine Mail-Adresse ändern?", antwort: "Du kannst deine Mail-Adresse in deinem Profil (oben rechts) durch die Eingabe deiner neuen Mail-Adresse im Mail-Adressfeld ändern." },
	{ id: 4, kategorie: "Kurse", frage: "Wie können Kurse hinzugefügt werden?", antwort: "Vermisst du einen Kurs in deiner Kursübersicht, kannst du uns gerne eine Nachricht über das Kontaktformular zukommen lassen. Teile uns hier mit, welchen Kurs du gerne hinzugefügt hättest." },
	{ id: 5, kategorie: "Fragenpool / Fragenwerkstatt", frage: "Wie funktioniert die Fragenwerkstatt?", antwort: "In der Fragenwerkstatt werden alle Fragen aufgeführt, die zu 30% negativ bewertet wurden. Alle Benutzer können diese Frage dann sehen und bearbeiten." },
	{ id: 6, kategorie: "Fragenpool / Fragenwerkstatt", frage: "Bleibt eine Frage in der Fragenwerkstatt, auch wenn sie verändert wurde?", antwort: "Die Likes / Dislikes einer Frage werden, nachdem sie in der Fragenwerkstatt bearbeitet wurde, zurückgesetzt." },
	{ id: 7, kategorie: "Joker", frage: "Wie funktioniert der Joker „50/50“?", antwort: "Wird der Joker „50/50“ aktiviert, wird die Hälfte der Antwortmöglichkeiten entfernt." },
    { id: 8, kategorie: "Joker", frage: "Wie funktioniert der Joker „Frage tauschen“?", antwort: "Wird der Joker „Frage tauschen“ aktiviert, wird die Frage gegen eine Frage des gleichen Schwierigkeitsgrades ausgetauscht." },
    { id: 9, kategorie: "Joker", frage: "Wie funktioniert der Joker „Hinweis“?", antwort: "Wird der Joker „Hinweis“ aktiviert, wird am Ende der Seite ein Tipp angezeigt, der die Beantwortung der Frage erleichtern kann." },
    { id: 10, kategorie: "Joker", frage: "Wie funktioniert der Joker „Sicherheitsnetz“?", antwort: "Wird der Joker „Sicherheitsnetz“ aktiviert, bekommt der Nutzer eine zweite Chance bei der Beantwortung der Frage, sollte er diese falsch beantworten." },
    { id: 11, kategorie: "Spielmodus: Quizshow", frage: "Sind die Fragen im Spielmodus Quizshow mit steigender Schwierigkeit?", antwort: "Die Fragen im Spielmodus Quizshow sind mit steigender Schwierigkeit." },
    { id: 12, kategorie: "Live-Event", frage: "Was passiert wenn ich mich nicht genau um 18 Uhr zum Live-Event anmelden kann?", antwort: "Du hast jeden Tag um 18 Uhr Zeit dich innerhalb von 5 Minuten für das Live-Event anzumelden." },
    { id: 13, kategorie: "Live-Event", frage: "Kann man die Profile von seinen Mitspielern sehen?", antwort: "Man kann die Profile von seinen Mitspielern im Live-Event nicht sehen - also keine Angst ;)" }
];

/**
 * Ein Array von Strings, die verschiedene Studienfachrichtungen repräsentieren.
 * Das Array wird verwendet um beim register die Kurse zuzuweisen.
 */
const fachrichtungen = [
	"Informatik",
	"Wirtschaftsinformatik",
	"Medieninformatik"
];

/**
 * Asynchrone Funktion, die aktuelle Benutzerdaten der REST API abruft.
 * Verwendet den im LocalStorage gespeicherten Zugriffstoken für die Authentifizierung.
 * @returns {Promise<Object>} Ein Promise-Objekt, das bei erfolgreicher Abfrage die Benutzerdaten im JSON-Format zurückgibt.
 * @throws {Error} Wirft einen Fehler, wenn die Netzwerkanfrage fehlschlägt oder der Server einen Fehlerstatus zurückgibt.
 */

async function fetchUserData() {
    const baseUrl = 'http://167.172.184.62/api/v1/current_user/';

    try {
        const accessToken = localStorage.getItem('access');
        const response = await fetch(baseUrl, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error(`Netzwerkantwort Kaputt: ${response.status}`);
        }

        return await response.json();
    } catch (error) {
        console.error('Fehler beim Abrufen der Benutzerdaten:', error);
        throw error;
    }
}

/**
 * Asynchrone Funktion, die eine Liste aller Kurse von der REST API abruft.
 * Erfordert einen gültigen Zugriffstoken im LocalStorage für die Authentifizierung.
 * @returns {Promise<Object[]>|undefined} Ein Promise-Objekt, das bei erfolgreicher Abfrage ein Array von Kursen im JSON-Format zurückgibt. Gibt `undefined` zurück, wenn kein Zugriffstoken gefunden wird.
 * @throws {Error} Wirft einen Fehler, wenn die Netzwerkanfrage fehlschlägt oder der Server einen Fehlerstatus zurückgibt.
 */
async function fetchAllKurse() {
    const accessToken = localStorage.getItem('access');
    if (!accessToken) {
        console.error('Kein Zugriffstoken gefunden.');
        return;
    }

    const baseUrl = 'http://167.172.184.62/api/v1/kurs/';
    try {
        const response = await fetch(baseUrl, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessToken}`
            }
        });

        if (!response.ok) {
            throw new Error(`Fehler bei der Anfrage: ${response.status}`);
        }

        const kurse = await response.json();
        return kurse;
    } catch (error) {
        console.error('Fehler beim Abrufen der Kurse:', error);
        throw error;
    }
}

/**
 * Asynchrone Funktion, die Statistiken zu Nutzerfragen von der REST API abruft.
 * Benötigt einen gültigen Zugriffstoken, der im LocalStorage gespeichert ist, für die Authentifizierung.
 * @returns {Promise<Object>|undefined} Ein Promise-Objekt, das bei erfolgreicher Abfrage Statistiken im JSON-Format zurückgibt. Gibt `undefined` zurück, wenn kein Zugriffstoken gefunden wird.
 * @throws {Error} Wirft einen Fehler, wenn die Netzwerkanfrage fehlschlägt oder der Server einen Fehlerstatus zurückgibt.
 */
async function fetchStats() {
    const accessToken = localStorage.getItem('access');
    if (!accessToken) {
        console.error('Kein Zugriffstoken gefunden.');
        return;
    }

    const baseUrl = 'http://167.172.184.62/api/v1/frage-nutzer-statistik/';
    try {
        const response = await fetch(baseUrl, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessToken}`
            }
        });

        if (!response.ok) {
            throw new Error(`Fehler bei der Anfrage: ${response.status}`);
        }

        const stats = await response.json();
        return stats;
    } catch (error) {
        console.error('Fehler beim Abrufen der Kurse:', error);
        throw error;
    }
}

/**
 * Asynchrone Funktion, die alle Fragen von der REST API abruft.
 * Verwendet den im LocalStorage gespeicherten Zugriffstoken für die Authentifizierung.
 * @returns {Promise<Object[]>} Ein Promise-Objekt, das bei erfolgreicher Abfrage ein Array von Fragen im JSON-Format zurückgibt.
 * @throws {Error} Wirft einen Fehler, wenn die Netzwerkanfrage fehlschlägt oder der Server einen Fehlerstatus zurückgibt.
 */
async function fetchAllQuestions() {
    const url = 'http://167.172.184.62/api/v1/frage/';

    try {
        const accessToken = localStorage.getItem('access'); 
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error(`Fehler bei der Anfrage: ${response.status}`);
        }

        return await response.json();
    } catch (error) {
        console.error('Fehler beim Abrufen der Fragen:', error);
        throw error;
    }
}

/**
 * Überprüft, ob eine E-Mail-Adresse gültig ist, basierend auf einer Liste erlaubter Domains und einem regulären Ausdruck.
 * Die Funktion teilt die E-Mail-Adresse am '@'-Zeichen und überprüft, ob die Domain in der Liste `validEmailDomains` enthalten ist.
 * Anschließend wird die gesamte E-Mail-Adresse gegen einen regulären Ausdruck getestet, um das Format zu überprüfen.
 *
 * @param {string} email Die zu überprüfende E-Mail-Adresse.
 * @returns {boolean} Gibt `true` zurück, wenn die E-Mail-Adresse eine gültige Domain hat und dem Format entspricht, sonst `false`.
 */
function validateEmail(email) {
    const domain = email.split('@')[1];
    return validEmailDomains.includes(domain) && /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(email);
}

/**
 * Validiert einen Benutzernamen basierend auf Längenbeschränkungen und dem Vorhandensein von Leerzeichen.
 * Ein gültiger Benutzername muss zwischen 5 und 25 Zeichen lang sein. Es darf maximal ein Leerzeichen zwischen den Zeichen sein.
 * Die Funktion überprüft die Länge des Benutzernamens und durchläuft ihn, um aufeinanderfolgende Leerzeichen zu identifizieren.
 * Abschließend wird ein regulärer Ausdruck verwendet, um sicherzustellen, dass der Benutzername nur Buchstaben, Zahlen und Leerzeichen enthält.
 *
 * @param {string} username Der zu überprüfende Benutzername.
 * @returns {boolean} Gibt `true` zurück, wenn der Benutzername den Kriterien entspricht, sonst `false`.
 */
function validateUsername(username) {
    if (username.length < 5 || username.length > 25) {
        return false;
    }

    let hasSpace = false;
    for (let i = 0; i < username.length; i++) {
        if (username[i] === ' ') {
            if (hasSpace) {
                return false; 
            }
            hasSpace = true;
        } else {
            hasSpace = false;
        }
    }

    return /^[A-Za-z0-9äöüß ]+$/.test(username);
}

/**
 * Überprüft die Gültigkeit eines Passworts basierend auf einem regulären Ausdruck.
 * Ein gültiges Passwort muss mindestens einen Buchstaben, eine Zahl und ein Sonderzeichen enthalten und darf nicht ausschließlich aus Zahlen bestehen.
 * Die Länge des Passworts muss mindestens 8 Zeichen betragen. Der reguläre Ausdruck prüft auf diese Anforderungen.
 *
 * @param {string} password Das zu überprüfende Passwort.
 * @returns {boolean} Gibt `true` zurück, wenn das Passwort den festgelegten Kriterien entspricht, sonst `false`.
 */
function validatePassword(password) {
    return /^(?=.*[A-Za-zäöüß])(?=.*\d)(?=.*[@$!%*?&#+-._])[A-Za-z\däöüß@$!%*?&#+-._]{8,}$/.test(password) && !/^\d+$/.test(password);
}

/**
 * Registriert einen Event-Listener, der auf das 'DOMContentLoaded' Ereignis wartet, um sicherzustellen, dass das DOM vollständig geladen ist.
 * Nach dem Laden des DOMs werden alle Container mit der Klasse `.thumbs-up` ausgewählt. Für jedes dieser Container-Elemente werden
 * Event-Listener auf die Klick-Ereignisse für die Daumen-hoch und Daumen-runter Icons gesetzt. Die Icons werden durch ihre Klassen
 * `.fa-thumbs-up` und `.fa-thumbs-down` identifiziert. Bei einem Klick auf eines der Icons wird die Funktion `handleThumbClick` aufgerufen.
 *
 * Voraussetzungen:
 * - Die Funktion `handleThumbClick` muss im Skript definiert sein, um die Klick-Ereignisse zu verarbeiten.
 * - Die Icons und Container müssen entsprechend mit den Klassen `.fa-thumbs-up`, `.fa-thumbs-down` und `.thumbs-up` markiert sein.
 */
document.addEventListener('DOMContentLoaded', () => {
    const thumbsUpContainers = document.querySelectorAll('.thumbs-up');

    thumbsUpContainers.forEach(container => {
        const thumbsUpIcon = container.querySelector('.fa-thumbs-up');
        const thumbsDownIcon = container.querySelector('.fa-thumbs-down');
        thumbsUpIcon.addEventListener('click', handleThumbClick);
        thumbsDownIcon.addEventListener('click', handleThumbClick);
    });
});

/**
 * Behandelt Klick-Ereignisse auf Daumen-hoch und Daumen-runter Icons. Bewertet eine Frage basierend auf dem angeklickten Icon
 * und zeigt eine entsprechende Nachricht an. Nach der Bewertung werden die Icons deaktiviert, um weitere Klicks zu verhindern.
 *
 * @param {Event} event - Das Klick-Ereignis, das diese Funktion auslöst.
 */
function handleThumbClick(event) {
    const container = event.target.closest('.thumbs-up');
    const questionId = event.target.getAttribute("data-question-id");
    if (questionId) {
        if (event.target.classList.contains('fa-thumbs-up')) {
            rateQuestion(questionId, "like");
            showMessage('Frage gefällt dir! \u263a', 1);
            event.target.classList.add('like-active');
        } else if (event.target.classList.contains('fa-thumbs-down')) {
            rateQuestion(questionId, "dislike");
            showMessage('Frage gefällt dir nicht! \u2639', 2);
            event.target.classList.add('dislike-active');
        }

        container.querySelectorAll('.fas').forEach(icon => {
            icon.removeEventListener('click', handleThumbClick);
            icon.classList.add('disabled');
        });
    } else {
        console.error('Keine Frage-ID gefunden');
    }
}


/**
 * Fügt Klick-Event-Listener zu Daumen-hoch und Daumen-runter Icons in allen `.thumbs-up` Containern hinzu.
 * Dies ermöglicht die Interaktion mit den Bewertungsicons, um Fragen zu bewerten.
 */
function addThumbClickListeners() {
    const thumbsUpContainers = document.querySelectorAll('.thumbs-up');

    thumbsUpContainers.forEach(container => {
        const thumbsUpIcon = container.querySelector('.fa-thumbs-up');
        const thumbsDownIcon = container.querySelector('.fa-thumbs-down');
        thumbsUpIcon.addEventListener('click', handleThumbClick);
        thumbsDownIcon.addEventListener('click', handleThumbClick);
    });
}

/**
 * Aktiviert die Daumen-hoch und Daumen-runter Icons, indem es die `disabled`, `like-active` und `dislike-active` Klassen entfernt
 * und die Event-Listener erneut hinzufügt. Dies wird typischerweise aufgerufen, um die Icons nach einer vorherigen Bewertung wieder nutzbar zu machen.
 */
function enableThumbs() {
    document.querySelectorAll('.thumbs-up .fas.disabled').forEach(icon => {
        icon.classList.remove('disabled');
        icon.classList.remove('like-active');
        icon.classList.remove('dislike-active');
    });
    addThumbClickListeners();
}

/**
 * Sendet eine asynchrone Anfrage an den Server, um eine Frage als "like" oder "dislike" zu bewerten, abhängig vom übergebenen Modus.
 *
 * @param {string} questionId - Die ID der Frage, die bewertet wird.
 * @param {"like"|"dislike"} mod - Der Bewertungsmodus, kann entweder "like" oder "dislike" sein.
 * @returns {Promise<Object>} Ein Promise, das bei Erfolg ein JSON-Objekt mit dem Ergebnis der Bewertung zurückgibt.
 * @throws {Error} Wirft einen Fehler, wenn die Netzwerkanfrage fehlschlägt oder der Server einen Fehlerstatus zurückgibt.
 */
async function rateQuestion(questionId, mod) {
    const accessToken = localStorage.getItem('access');
    const url = `http://167.172.184.62/api/v1/frage/${questionId}/${mod}/`;

    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error(`Netzwerkantwort war nicht ok: ${response.status}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Fehler beim Disliken der Frage:', error);
        throw error;
    }
}

/**
 * Sendet eine asynchrone POST-Anfrage an eine bestimmte URL, um eine Aktion innerhalb eines Kurses die Statistik anzupassen.
 *
 * @param {string} kursId - Die ID des Kurses, für den die Aktion verzeichnet wird.
 * @param {string} mode - Der Modus der Aktion, die verzeichnet wird.
 * @returns {Promise<Object>|undefined} - Gibt ein Promise-Objekt zurück, das bei Erfolg die Antwort des Servers im JSON-Format enthält. Bei einem Fehler wird `undefined` zurückgegeben.
 */
async function statAnswer(kursId, mode) {
    const accessToken = localStorage.getItem('access');
    const url = `http://167.172.184.62/api/v1/beantworten/${kursId}/${mode}/`;

    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessToken}`
            }
        });

        if (!response.ok) {
            throw new Error(`Anfrage fehlgeschlagen: ${response.status}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
    }
}

/**
 * Zeigt eine Nachricht für eine bestimmte Zeit auf dem Bildschirm an. Die Nachricht und ihr Typ (Erfolg oder Fehler) werden als Parameter übergeben.
 *
 * @param {string} message - Die anzuzeigende Nachricht.
 * @param {number} type - Der Typ der Nachricht (1 für Erfolg, 2 für Fehler).
 */
function showMessage(message, type) {
	const messageDiv = document.createElement('div');
	messageDiv.textContent = message;
	messageDiv.classList.add('message-div');

	if (type === 1) {
		messageDiv.classList.add('success');
	} else if (type === 2) {
		messageDiv.classList.add('error');
	}

	document.body.appendChild(messageDiv);

	setTimeout(() => {
		messageDiv.remove();
	}, 4000); 
}

/**
 * Extrahiert und gibt den Wert einer spezifischen Query-Variable aus der URL zurück.
 *
 * @param {string} variable - Der Name der Query-Variable, deren Wert zurückgegeben werden soll.
 * @returns {string|null} - Den Wert der Query-Variable, wenn vorhanden, andernfalls `null`.
 */
function getQueryVariable(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		if (decodeURIComponent(pair[0]) === variable) {
			return decodeURIComponent(pair[1]);
		}
	}
	return null;
}

/**
 * Aktualisiert den Inhalt der Titelbox basierend auf der Kurs-ID, die aus der URL als Query-Variable extrahiert wird.
 * Fordert eine Liste aller Kurse an und findet den entsprechenden Kurs, um seinen Code im Titel anzuzeigen.
 */
async function updateTitleBox() {
	const kursId = getQueryVariable("kurs_id");
	kurse = await fetchAllKurse();
	if (kursId) {
		const kurs = kurse.find(k => k.id.toString() === kursId);
		if (kurs) {
			document.querySelector('.title-box p').textContent = kurs.code;
		}
	}
}

/**
 * Aktualisiert den Modus in der Titelbox basierend auf dem Wert der Query-Variable `modus` aus der URL.
 */
function updateModus() {
	const modus = getQueryVariable("modus");
	if (modus) {
		document.querySelector('.title-box h1').textContent = modus;
	}
}

/**
 * Ein Event-Listener, der auf das 'DOMContentLoaded' Ereignis wartet. Initialisiert die Animation für ein Marquee-Textelement,
 * indem der Text auf Basis seiner Breite und der Breite seines Containers animiert wird.
 * + cookie logik
 */
document.addEventListener('DOMContentLoaded', function() {
    const consentPropertyName = 'user_cookie_consent';
    console.log(getCookie(consentPropertyName));
    darkElements = document.getElementsByClassName('dark-mode-toggle-container');

    if (getCookie(consentPropertyName) == null) {
        const consentPopup = document.getElementById('cookieConsentContainer');
        const acceptBtn = document.getElementById('acceptCookieConsent');
        const declineBtn = document.getElementById('declineCookieConsent');

        if (darkElements) {
            for (var i = 0; i < darkElements.length; i++) {
                darkElements[i].style.display = 'none';
            }
        }

        consentPopup.style.display = 'flex';

        acceptBtn.addEventListener('click', () => {
            setCookie(consentPropertyName, 'true', 365);
            consentPopup.style.display = 'none';
            if (darkElements) {
                for (var i = 0; i < darkElements.length; i++) {
                    darkElements[i].style.display = 'flex';
                }
            }
        });

        declineBtn.addEventListener('click', () => {
            setCookie(consentPropertyName, 'false', 365);
            consentPopup.style.display = 'none';
        });
    }

    if (getCookie(consentPropertyName) == 'false') {
        if (darkElements) {
            for (var i = 0; i < darkElements.length; i++) {
                darkElements[i].style.display = 'none';
            }
        }
    }


    const marqueeText = document.getElementById('marqueeText');
    if (marqueeText) {
        let updateWidths = function() {
            marqueeWidth = marqueeText.offsetWidth;
            containerWidth = marqueeText.parentElement.offsetWidth;
        };

        let marqueeWidth, containerWidth, startPosition, endPosition;
        updateWidths();

        window.addEventListener('resize', function() {
            updateWidths();
            startPosition = containerWidth;
            endPosition = -marqueeWidth;
        });

        startPosition = containerWidth;
        endPosition = -marqueeWidth;

        function textAuswahl(faqs) {
            let randomIndex = Math.floor(Math.random() * faqs.length);
            return faqs[randomIndex].antwort;
        }

        let ausgewaehlteAntwort = textAuswahl(faqs);
        marqueeText.innerHTML = ausgewaehlteAntwort;

        function animateMarquee() {
            startPosition--;
            if (startPosition < endPosition) {
                startPosition = containerWidth;
                marqueeText.innerHTML = textAuswahl(faqs);
            }
            marqueeText.style.left = startPosition + 'px';
            requestAnimationFrame(animateMarquee);
        }

        animateMarquee();
    }
});


// Funktion zum Setzen eines Cookies
function setCookie(name, value, days) {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

// Funktion zum Setzen eines Cookies, wenn nicht bereits gesetzt
function setCookieIfNotSet(name, value, days) {
    const existingValue = getCookie(name);
    if (existingValue === null) {
        setCookie(name, value, days);
    }
}

// Funktion zum Abrufen eines Cookies
function getCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for(let i=0;i < ca.length;i++) {
        let c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

/**
 * Generiert eine Liste gültiger E-Mail-Domains basierend auf vordefinierten Präferenzen und Domain-Mustern.
 * Die Präferenzen und Muster werden durch die Arrays `emailPref` und `emailDomains` definiert.
 * Das Ergebnis ist eine Liste von kombinierten, gültigen E-Mail-Domains, gespeichert in `validEmailDomains`.
 */
const emailPref = ['iu', 'iubh'];
const emailDomains = ['PREF.de', 'PREF.org', 'PREF-fernstudium.de', 'PREF-fernstudium.org', 'PREF-study.org'];

const validEmailDomains = [];

emailDomains.forEach(domain => {
    emailPref.forEach(prefix => {
        const updatedDomain = domain.replace('PREF', prefix);
        validEmailDomains.push(updatedDomain);
    });
});

document.addEventListener('DOMContentLoaded', function() {

});



