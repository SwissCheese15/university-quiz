/**
 * @file Logik für die Kursübersicht
 */


/**
 * Erstellt asynchron eine Übersicht der Kurse und bindet sie in die Webseite ein.
 * Diese Funktion versucht, eine Liste aller verfügbaren Kurse zu laden und für jeden Kurs
 * ein entsprechendes Element im DOM zu erzeugen. Diese Elemente werden dann in einem
 * Container-Element (#kursuebersicht) zusammengefasst und auf der Webseite dargestellt.
 * Bei einem Fehler wird eine entsprechende Meldung in der Konsole ausgegeben.
 */
async function erstelleKursuebersicht() {
    try {
        // Erstellt das Hauptcontainer-Element für die Kursübersicht.
        const kursuebersicht = document.createElement('div');
        kursuebersicht.id = 'kursuebersicht';

        // Lädt alle Kurse asynchron.
        const kurse = await fetchAllKurse();

        // Überprüft, ob Kurse vorhanden sind und erstellt für jeden Kurs ein DOM-Element.
        if (kurse && kurse.length > 0) {
            let umgekehrteKurse = [...kurse].reverse();
            umgekehrteKurse.forEach(kurs => {
                // Erstellt ein Div-Element für den Kurs.
                const kursDiv = document.createElement('div');
                kursDiv.className = 'kurs';
                kursDiv.setAttribute('data-kurs-id', kurs.id);

                // Fügt ein Bild des Kurses hinzu.
                const kursBild = document.createElement('img');
                kursBild.src = `media/img/kurs/${kurs.code}.png`;
                kursBild.alt = `${kurs.code}`;
                kursDiv.appendChild(kursBild);

                // Erstellt einen Container für den Kursnamen und das Kürzel.
                const textContainer = document.createElement('div');
                textContainer.className = 'kurs-text-container';

                // Fügt den Namen des Kurses hinzu.
                const kursName = document.createElement('h3');
                kursName.textContent = kurs.name;
                textContainer.appendChild(kursName);

                // Fügt das Kürzel des Kurses hinzu.
                const kursKuerzel = document.createElement('p');
                kursKuerzel.textContent = kurs.code;
                textContainer.appendChild(kursKuerzel);

                kursDiv.appendChild(textContainer);

                // Fügt einen Event-Listener hinzu, um bei Klick eine Detailseite des Kurses zu öffnen.
                kursDiv.addEventListener('click', function() {
                    window.location.href = `kurs.html?kurs_id=${this.getAttribute('data-kurs-id')}`;
                });

                kursuebersicht.appendChild(kursDiv);
            });

            // Fügt die Kursübersicht dem Container auf der Webseite hinzu.
            document.getElementById('kursuebersicht-container').appendChild(kursuebersicht);
        } else {
        }
    } catch (error) {
        console.error('Fehler beim Erstellen der Kursübersicht:', error);
    }
}

// Fügt einen Event-Listener hinzu, um die Funktion erstelleKursuebersicht auszuführen, sobald das DOM geladen ist.
document.addEventListener('DOMContentLoaded', erstelleKursuebersicht);