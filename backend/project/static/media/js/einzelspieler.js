/**
 * @file Logik für Navigation im Einzelspieler
 */

/**
 * Überprüft asynchron, ob für einen gegebenen Kurs eine Mindestanzahl von Fragen vorhanden ist.
 * @param {string} kursId - Die ID des Kurses, für den die Fragenanzahl überprüft wird.
 * @returns {Promise<boolean>} - Ein Promise, das true zurückgibt, wenn mindestens 11 Fragen für den Kurs vorhanden sind, andernfalls false.
 */
async function checkFragenAnzahl(kursId) {
    // Lädt alle Fragen asynchron.
    fragen = await fetchAllQuestions();
    // Filtert die Fragen, die zum spezifischen Kurs gehören.
    const fragenFuerKurs = fragen.filter(frage => frage.kurs == kursId);
    // Überprüft, ob die Anzahl der relevanten Fragen mindestens 11 beträgt.
    return fragenFuerKurs.length >= 11;
}

/**
 * Führt verschiedene Initialisierungen aus, sobald das DOM geladen ist, und entscheidet,
 * ob der Lernmodus oder die Quizshow basierend auf der Verfügbarkeit der Fragen für einen Kurs angezeigt wird.
 * Die Entscheidung basiert auf der Anzahl der Fragen, die für den gewählten Kurs verfügbar sind.
 */
document.addEventListener('DOMContentLoaded', async function() {
    // Extrahiert die URL-Parameter.
    var urlParams = new URLSearchParams(window.location.search);
    var modus = urlParams.get('modus');
    var kursId = urlParams.get('kurs_id');

    // Initialisiert die Titelbox und den Modus.
    updateTitleBox();
    updateModus();

    // Überprüft asynchron die Anzahl der Fragen für den gewählten Kurs.
    let check = await checkFragenAnzahl(kursId);

    // Entscheidet basierend auf der Verfügbarkeit der Fragen, welcher Modus angezeigt wird.
    if (check) {
        if (modus === 'Lernmodus') {
            document.getElementById('modus_lernmodus').style.display = 'block';
            document.getElementById('modus_quizshow').style.display = 'none';
        } else if (modus === 'Quizshow') {
            document.getElementById('modus_quizshow').style.display = 'flex';
            document.getElementById('modus_lernmodus').style.display = 'none';
        }
    } else {
        // Zeigt eine Meldung an, falls nicht genügend Fragen verfügbar sind.
        document.getElementById('zuWenigFragen').style.display = 'block';
    }
});
