/**
 * @file Logik für die FAQs
 */

/**
 * Fügt der Webseite interaktive FAQ-Abschnitte hinzu, sobald das DOM vollständig geladen ist.
 * Die FAQs werden aus einer globalen `faqs` Variable gelesen, die ein Array von FAQ-Objekten enthält.
 * Jedes FAQ-Objekt muss mindestens eine `kategorie`, `frage` und `antwort` Eigenschaft haben.
 */
document.addEventListener('DOMContentLoaded', function() {
    // Findet den Container, in dem die FAQ-Kategorien angezeigt werden sollen.
    const faqContainer = document.getElementById('faq-container');

    // Gruppiert die FAQs nach ihrer Kategorie.
    const groupedFaqs = faqs.reduce((acc, faq) => {
        if (!acc[faq.kategorie]) acc[faq.kategorie] = [];
        acc[faq.kategorie].push(faq);
        return acc;
    }, {});

    // Erstellt für jede Kategorie und die dazugehörigen FAQs DOM-Elemente.
    Object.entries(groupedFaqs).forEach(([kategorie, faqList], index) => {
        // Erstellt und konfiguriert das Div für die FAQ-Kategorie.
        const categoryDiv = document.createElement('div');
        categoryDiv.className = 'faq-category';                                             
        categoryDiv.innerHTML = `<div class='faq-category-title'>${kategorie}</div>`;       

        // Erstellt ein Container-Div für die Fragen dieser Kategorie.
        const questionsDiv = document.createElement('div');
        questionsDiv.className = 'faq-questions';
        questionsDiv.id = `faq-questions-${index}`;
        questionsDiv.style.display = index === 0 ? 'block' : 'none';

        // Fügt jede Frage und Antwort als Kind-Elemente zu `questionsDiv` hinzu.
        faqList.forEach(faq => {
            const questionDiv = document.createElement('div');
            questionDiv.className = 'faq-question';
            questionDiv.innerHTML = `<div class='faq-question-title'>${faq.frage}</div>`;

            const answerDiv = document.createElement('div');
            answerDiv.className = 'faq-answer';
            answerDiv.innerHTML = faq.antwort;

            questionDiv.appendChild(answerDiv);
            questionsDiv.appendChild(questionDiv);

            // Fügt einen Klick-Event-Listener hinzu, um die Antwort ein-/auszublenden.
            questionDiv.addEventListener('click', function() {
                answerDiv.classList.toggle('active');
                const currentQuestionTitle = questionDiv.querySelector('.faq-question-title');
                currentQuestionTitle.classList.toggle('active');      
            });
        });

        // Markiert den Titel der ersten Kategorie standardmäßig als aktiv.
        if(index === 0) {
            const firstCategoryTitle = categoryDiv.querySelector('.faq-category-title');
            firstCategoryTitle.classList.add('active');
        }

        // Fügt die Fragen zur Kategorie hinzu und dann die Kategorie zum Hauptcontainer.
        categoryDiv.appendChild(questionsDiv);
        faqContainer.appendChild(categoryDiv);

        // Fügt einen Klick-Event-Listener hinzu, um die Fragen der Kategorie ein-/auszublenden.
        categoryDiv.querySelector('.faq-category-title').addEventListener('click', function() {
            questionsDiv.style.display = questionsDiv.style.display === 'none' ? 'block' : 'none';
            this.classList.toggle('active');
        });        
    });
});
