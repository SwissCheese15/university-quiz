/**
 * @file Logik für einen dynamischen Header und Footer
 */

/**
 * Setzt den Inhalt des Header- und Footer-Bereichs einer Webseite dynamisch.
 * Diese Funktion wird ausgeführt, sobald das DOM geladen ist, und aktualisiert den Header- und
 * Footer-Bereich mit HTML-Strukturen, die das Layout und die Navigation der Seite definieren.
 * Sie integriert externe Stylesheets und Icons zur Gestaltung des Headers und Footers.
 */

function setFooterHeader(){
    // Überprüft, ob der Dark Mode aktiv ist
    let darkMode = getCookie('dark') === 'true';

    // Setzt den Inhalt des Headers.
    const header = document.getElementById('header');
    header.innerHTML = `
    <meta charset="UTF-8">
    <link rel="stylesheet" href="media/css/${darkMode ? 'dark/header_dark' : 'header'}.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <header>
      <div class="header-content">
        <a href="index.html">
            <img src="media/img/logo-header.png" alt="QuizzIU Logo" class="header-logo">
        </a>
        <nav class="header-nav">
          <a href="index.html" class="nav-button"><i class="fas fa-graduation-cap"></i> Kursübersicht</a>
          <a href="profil.html" class="nav-button"><i class="fas fa-user"></i> Profil</a>
          <a href="faq.html" class="nav-button"><i class="fas fa-question-circle"></i> FAQ</a>
        </nav>
      </div>
      <div class="decorative-bar"></div>
    </header>
    `;

    // Setzt den Inhalt des Footers.
    const footer = document.getElementById('footer');
    footer.innerHTML = `
    <link rel="stylesheet" href="media/css/${darkMode ? 'dark/footer_dark' : 'footer'}.css">
    <footer>
        <a href="https://mycampus.iu.org/data-privacy" target="_blank" >Datenschutz</a>
        <a href="https://mycampus.iu.org/legal" target="_blank" >Impressum</a>
        <a href="https://www.iu.de/hochschule/studierendensekretariat/" target="_blank" >Kontakt</a>
        <a href="ueber-uns.html" >Über uns</a>
    </footer>
    `;
}

document.addEventListener('DOMContentLoaded', function() {
    setFooterHeader()
});
