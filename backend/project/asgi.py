"""
ASGI config for djangoProject project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter, URLRouter
from api.routing import websocket_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')

# Get the default ASGI application for regular HTTP requests
django_asgi_app = get_asgi_application()

# Combine both regular HTTP and WebSocket applications
application = ProtocolTypeRouter({
    "http": django_asgi_app,  # Use the default ASGI application for HTTP
    "websocket": websocket_application,  # Use the WebSocket application for WebSocket connections
})
