from .base import *
import datetime

DEBUG = False
ALLOWED_HOSTS = ['167.172.184.62', 'localhost', '127.0.0.1', 'http://jurio.eu', 'jurio.eu']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': datetime.timedelta(days=2),
    'REFRESH_TOKEN_LIFETIME': datetime.timedelta(days=10)
}

CORS_ALLOWED_ORIGINS = [
    "http://quizziu.de",
    "https://quizziu.de",
]