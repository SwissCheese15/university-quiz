import datetime
from .base import *

DEBUG = True
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',  # same as POSTGRES_DB in docker-compose
        'USER': 'postgres',  # same as POSTGRES_USER in docker-compose
        'PASSWORD': 'postgres',  # same as POSTGRES_PASSWORD in docker-compose
        'HOST': 'postgres',  # name of the service in docker-compose
        'PORT': '5432',  # default PostgreSQL port
    }
}

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': datetime.timedelta(days=10),
    'REFRESH_TOKEN_LIFETIME': datetime.timedelta(days=12)
}

CORS_ALLOW_ALL_ORIGINS = True
