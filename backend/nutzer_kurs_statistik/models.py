from django.db import models
from custom_auth.models import CustomUser
from kurse.models import Kurs

class NutzerKursStatistik(models.Model):
    id = models.AutoField(primary_key=True)
    kurs = models.ForeignKey(Kurs, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    lernmodus_richtig = models.IntegerField(default=0)
    lernmodus_falsch = models.IntegerField(default=0)
    quizshow_richtig = models.IntegerField(default=0)
    quizshow_falsch = models.IntegerField(default=0)
    liveevent_richtig = models.IntegerField(default=0)
    liveevent_falsch = models.IntegerField(default=0)

    def __str__(self):
        return f"ID: {self.id} - User: {self.user.email} - Kurs: {self.kurs.code}"

    class Meta:
        verbose_name_plural = 'Nutzer-Kurs Statistiken'
        unique_together = ['user', 'kurs']