from rest_framework import viewsets
from drf_spectacular.utils import extend_schema
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .models import NutzerKursStatistik
from .serializers import NutzerKursStatistikSerializer
from custom_auth.permissions import IsAdminOrTheUser


@extend_schema(tags=["Nutzer Kurs Statistik"])
class NutzerKursStatistikViewSet(viewsets.ModelViewSet):
    queryset = NutzerKursStatistik.objects.all()
    serializer_class = NutzerKursStatistikSerializer
    permission_classes = [IsAdminOrTheUser]


@extend_schema(tags=["Fragen Beantworten"])
class FrageBeantwortenViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]

    def find_or_create_statistik(self, kurs_id, user):
        try:
            statistic = NutzerKursStatistik.objects.get(kurs_id=kurs_id, user=user)
        except NutzerKursStatistik.DoesNotExist:
            statistic = NutzerKursStatistik.objects.create(kurs_id=kurs_id, user=user)
        return statistic

    @extend_schema(summary="Increment lernmodus_richtig by 1")
    @action(detail=True, methods=['post'])
    def increment_lernmodus_richtig(self, request, kurs_id=None):
        statistic = self.find_or_create_statistik(kurs_id, request.user)
        statistic.lernmodus_richtig += 1
        statistic.save()
        serializer = NutzerKursStatistikSerializer(statistic)
        return Response(serializer.data)

    @extend_schema(summary="Increment lernmodus_falsch by 1")
    @action(detail=True, methods=['post'])
    def increment_lernmodus_falsch(self, request, kurs_id=None):
        statistic = self.find_or_create_statistik(kurs_id, request.user)
        statistic.lernmodus_falsch += 1
        statistic.save()
        serializer = NutzerKursStatistikSerializer(statistic)
        return Response(serializer.data)

    @extend_schema(summary="Increment quizshow_richtig by 1")
    @action(detail=True, methods=['post'])
    def increment_quizshow_richtig(self, request, kurs_id=None):
        statistic = self.find_or_create_statistik(kurs_id, request.user)
        statistic.quizshow_richtig += 1
        statistic.save()
        serializer = NutzerKursStatistikSerializer(statistic)
        return Response(serializer.data)

    @extend_schema(summary="Increment quizshow_falsch by 1")
    @action(detail=True, methods=['post'])
    def increment_quizshow_falsch(self, request, kurs_id=None):
        statistic = self.find_or_create_statistik(kurs_id, request.user)
        statistic.quizshow_falsch += 1
        statistic.save()
        serializer = NutzerKursStatistikSerializer(statistic)
        return Response(serializer.data)

    @extend_schema(summary="Increment liveevent_richtig by 1")
    @action(detail=True, methods=['post'])
    def increment_liveevent_richtig(self, request, kurs_id=None):
        statistic = self.find_or_create_statistik(kurs_id, request.user)
        statistic.liveevent_richtig += 1
        statistic.save()
        serializer = NutzerKursStatistikSerializer(statistic)
        return Response(serializer.data)

    @extend_schema(summary="Increment liveevent_falsch by 1")
    @action(detail=True, methods=['post'])
    def increment_liveevent_falsch(self, request, kurs_id=None):
        statistic = self.find_or_create_statistik(kurs_id, request.user)
        statistic.liveevent_falsch += 1
        statistic.save()
        serializer = NutzerKursStatistikSerializer(statistic)
        return Response(serializer.data)