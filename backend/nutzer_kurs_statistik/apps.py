from django.apps import AppConfig


class NutzerKursStatistikConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nutzer_kurs_statistik'
