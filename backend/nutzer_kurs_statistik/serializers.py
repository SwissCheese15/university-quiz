from rest_framework import serializers
from .models import NutzerKursStatistik


class NutzerKursStatistikSerializer(serializers.ModelSerializer):
    class Meta:
        model = NutzerKursStatistik
        fields = '__all__'