from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models

class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError('The Email field must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self.create_user(email, password, **extra_fields)

class CustomUser(AbstractBaseUser, PermissionsMixin):
    PHOTO_CHOICES = (
        ('PHOTO_ONE', 'Photo One'),
        ('PHOTO_TWO', 'Photo Two'),
        ('PHOTO_THREE', 'Photo Three'),
        ('PHOTO_FOUR', 'Photo Four'),
        ('PHOTO_FIVE', 'Photo Five'),
        ('PHOTO_SIX', 'Photo Six'),
    )

    email = models.EmailField(unique=True)
    username = models.CharField(max_length=30)
    photo = models.CharField(max_length=20, choices=PHOTO_CHOICES, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.email

    class Meta:
        verbose_name_plural = 'Nutzer'