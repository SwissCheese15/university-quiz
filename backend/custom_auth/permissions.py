from rest_framework.permissions import BasePermission


class IsAdminOrTheUser(BasePermission):
    def has_object_permission(self, request, view, obj):

        # Check if the requesting user is an admin
        if request.user.is_staff:
            return True

        # Check if the requesting user is the user itself
        return obj == request.user