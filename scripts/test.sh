python -c "import time; time.sleep(3)" # Wait for postgres to start up
python manage.py collectstatic --no-input
python manage.py makemigrations
python manage.py migrate
gunicorn --log-level debug project.wsgi:application --bind 0.0.0.0:8000