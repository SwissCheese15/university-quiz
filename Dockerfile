FROM python:3.9

# Set environment variables
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

# Install channels_redis and other dependencies globally
RUN pip install --upgrade channels_redis asgiref

# Create and set the working directory
WORKDIR /app/backend

# Copy the Pipfile and Pipfile.lock and install dependencies
COPY Pipfile Pipfile.lock /app/
RUN pip install pipenv
RUN pipenv install --system --deploy --ignore-pipfile

# Install Gunicorn
RUN pip install gunicorn

# Copy the contents of the backend directory to /app
COPY backend /app/backend
COPY scripts /app/scripts

# Expose port 8000 for Daphne
EXPOSE 8000

# Command to start the application with Daphne
CMD ["daphne", "-b", "0.0.0.0", "-p", "8000", "project.asgi:application"]

